import lockr from 'lockr'
import axios from 'axios'

export default class AuthService {
    setUser (data) {
        lockr.set('user', data)

        return this
    }

    setToken (data) {
        lockr.set('token', data)

        return this
    }

    getUser () {
        return lockr.get('user', false)
    }

    getRoleUserRole () {
        const user = this.getUser()

        try {
            return user.roles[0]
        } catch (e) {
            return {}
        }

    }

    getToken () {
        return lockr.get('token', false)
    }

    isLoggedIn () {
        return this.getToken()
    }

    refresh (success, error) {
        axios.post('/api/auth/refresh')
            .then(success)
            .catch(error)
    }

    logOut (invalidate, cl) {
        if (invalidate) {
            axios.post('/api/auth/logout')
                .then(() => {
                    lockr.rm('user')
                    lockr.rm('token')
                })
        } else {
            lockr.rm('user')
            lockr.rm('token')
        }

        cl()
    }
}
