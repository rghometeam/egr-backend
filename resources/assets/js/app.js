require('./bootstrap');
require('./particles.min');
require('./art');

window.Vue = require('vue');

import BootstrapVue from 'bootstrap-vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import store from './stores/index'
import AuthService from './services/Auth'
// import VueParticles from 'vue-particles'
import VueClipboard from 'vue-clipboard2'
import Bars from 'vuebars'
import Paginate from 'vuejs-paginate'
import VueToastr from '@deveodk/vue-toastr'

import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css'

Vue.use(VueToastr)
Vue.component('paginate', Paginate)
Vue.use(Bars)
Vue.use(VueClipboard)
Vue.use(BootstrapVue)
Vue.use(VueRouter)
// Vue.use(VueParticles);

let authService = new AuthService()

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
axios.defaults.headers.common['Content-Type'] = 'application/json'


import BuilderIndex from './pages/Builder/Index'
import BuilderCreate from './pages/Builder/Create'
import UsersIndex from './pages/Users/Index'
import UsersCreate from './pages/Users/Create'
import AddressesIndex from './pages/Addresses/Index'
import AddressesCreate from './pages/Addresses/Create'
import PackagesIndex from './pages/Packages/Index'
import PackagesCreate from './pages/Packages/Create'
import BlackListIndex from './pages/BlackList/Index'
import BlackListCreate from './pages/BlackList/Create'
import SettingsIndex from './pages/Settings/Index'
import SettingsCreate from './pages/Settings/Create'
import EmailDetailsEdit from './pages/EmailDetails/Edit'
import EmailDetailsIndex from './pages/EmailDetails/Index'
import Logs from './pages/Logs/Index'
import TelegramAuthIndex from './pages/TelegramAuth/Index'
import TelegramImportIndex from './pages/TelegramImport/Index'
import FaqUserIndex from './pages/FaqUser/Index'
import FaqAdminIndex from './pages/FaqAdmin/Index'
import FaqAdminCreate from './pages/FaqAdmin/Create'

import DashboardIndex from './pages/Dashboard/Index'
import ReferralsIndex from './pages/Referrals/Index'
import WithdrawalIndex from './pages/Withdrawal/Index'
import TransactionsIndex from './pages/Transactions/Index'
import TransactionsEdit from './pages/Transactions/Edit'

import Login from './pages/Auth/Login'
import LoginOld from './pages/Auth/LoginOld'
import Register from './pages/Auth/Register'
import Profile from './pages/Auth/Profile'
import Confirmation from './pages/Auth/Confirmation'
import Reset from './pages/Auth/Reset'
import NewPassword from './pages/Auth/NewPassword'

import Matrix from './components/Matrix'

import Sidebar from './components/Sidebar'
// import Main from './components/Main'

/**
 * Pages
 */
Vue.component('BuilderIndex', BuilderIndex);
Vue.component('BuilderCreate', BuilderCreate);
Vue.component('UsersIndex', UsersIndex);
Vue.component('UsersCreate', UsersCreate);
Vue.component('PackagesIndex', PackagesIndex);
Vue.component('PackagesCreate', PackagesCreate);
Vue.component('BlackListIndex', BlackListIndex);
Vue.component('BlackListCreate', BlackListCreate);
Vue.component('SettingsIndex', SettingsIndex);
Vue.component('SettingsCreate', SettingsCreate);
Vue.component('EmailDetailsIndex', EmailDetailsIndex);
Vue.component('EmailDetailsEdit', EmailDetailsEdit);
Vue.component('Logs', Logs);
Vue.component('TelegramImportIndex', TelegramImportIndex);
Vue.component('FaqUserIndex', FaqUserIndex);
Vue.component('FaqAdminIndex', FaqAdminIndex);
Vue.component('FaqAdminCreate', FaqAdminCreate);
Vue.component('AddressesIndex', AddressesIndex);
Vue.component('AddressesCreate', AddressesCreate);

Vue.component('DashboardIndex', DashboardIndex);
Vue.component('TransactionsIndex', TransactionsIndex);
Vue.component('ReferralsIndex', ReferralsIndex);
Vue.component('WithdrawalIndex', WithdrawalIndex);
Vue.component('TransactionsEdit', TransactionsEdit);

Vue.component('login', Login);
Vue.component('LoginOld', LoginOld);
Vue.component('register', Register);
Vue.component('Profile', Profile);
Vue.component('Confirmation', Confirmation);
Vue.component('Reset', Reset);
Vue.component('NewPassword', NewPassword);

Vue.component('Matrix', Matrix);

/**
 * Components
 */
Vue.component('sidebar', Sidebar);
// Vue.component('main', Main);

const router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'index',
            redirect: '/builder',
            component: {
                render (c) { return c('router-view') }
            },

            children: [
                {
                    path: 'builder',
                    name: 'builder',
                    component: BuilderIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'telegram-auth',
                    name: 'telegram',
                    component: TelegramAuthIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'telegram-import',
                    name: 'TelegramImport',
                    component: TelegramImportIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'builder/edit/:id',
                    name: 'edit',
                    component: BuilderCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'builder/create',
                    name: 'create',
                    component: BuilderCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'addresses',
                    name: 'addresses',
                    component: AddressesIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'addresses/create',
                    name: 'AddressesCreate',
                    component: AddressesCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'addresses/edit/:id',
                    name: 'AddressesEdit',
                    component: AddressesCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },

                {
                    path: 'users',
                    name: 'users',
                    component: UsersIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'users/create',
                    name: 'UsersCreate',
                    component: UsersCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'users/edit/:id',
                    name: 'UsersEdit',
                    component: UsersCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'logs',
                    name: 'logs',
                    component: Logs,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'faq-user',
                    name: 'faq-user',
                    component: FaqUserIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN',
                            'ROLE_USER',
                            'ROLE_PREMIUM',
                            'ROLE_FREE'
                        ]
                    }
                },
                {
                    path: 'faq-admin',
                    name: 'faq-admin',
                    component: FaqAdminIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN',
                        ]
                    }
                },
                {
                    path: 'faq-admin/create',
                    name: 'faq-admin-create',
                    component: FaqAdminCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN',
                        ]
                    }
                },
                {
                    path: 'faq-admin/edit/:id',
                    name: 'faq-admin-edit',
                    component: FaqAdminCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN',
                        ]
                    }
                },
                {
                    path: 'packages',
                    name: 'packages',
                    component: PackagesIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'packages/create',
                    name: 'PackagesCreate',
                    component: PackagesCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },

                {
                    path: 'packages/edit/:id',
                    name: 'PackagesEdit',
                    component: PackagesCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },

                {
                    path: 'black-list',
                    name: 'black-list',
                    component: BlackListIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'black-list/create',
                    name: 'BlackListCreate',
                    component: BlackListCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'black-list/edit/:id',
                    name: 'BlackListEdit',
                    component: BlackListCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'profile',
                    name: 'Profile',
                    component: Profile,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN',
                            'ROLE_USER',
                            'ROLE_PREMIUM',
                            'ROLE_FREE'
                        ]
                    }
                },

                {
                    path: 'dashboard',
                    name: 'DashboardIndex',
                    component: DashboardIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN',
                            'ROLE_USER',
                            'ROLE_PREMIUM',
                            'ROLE_FREE'
                        ]
                    }
                },

                {
                    path: 'settings',
                    name: 'settings',
                    component: SettingsIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'transactions',
                    name: 'transactions',
                    component: TransactionsIndex,
                    meta: {
                        guarded: true,
                    }
                },
                {
                    path: 'referrals',
                    name: 'referrals',
                    component: ReferralsIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_USER',
                            'ROLE_PREMIUM',
                            'ROLE_FREE'
                        ]
                    }
                },
                {
                    path: 'withdrawal',
                    name: 'withdrawal',
                    component: WithdrawalIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_USER',
                            'ROLE_PREMIUM',
                            'ROLE_FREE'
                        ]
                    }
                },
                {
                    path: 'transactions/edit/:id',
                    name: 'TransactionsEdit',
                    component: TransactionsEdit,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'settings/edit/:id',
                    name: 'SettingsEdit',
                    component: SettingsCreate,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'email-details',
                    name: 'EmailDetailsIndex',
                    component: EmailDetailsIndex,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
                {
                    path: 'email-details/edit/:id',
                    name: 'EmailDetailsEdit',
                    component: EmailDetailsEdit,
                    meta: {
                        guarded: true,
                        roles: [
                            'ROLE_ADMIN'
                        ]
                    }
                },
            ],
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/login-old',
            name: 'login-old',
            component: LoginOld,
        },
        {
            path: '/reset',
            name: 'reset',
            component: Reset,
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
        },
        {
            path: '/confirmation/:id/:token',
            name: 'Confirmation',
            component: Confirmation,
        },
        {
            path: '/new-password/:id/:token',
            name: 'NewPassword',
            component: NewPassword,
        },
    ],
});

/**
 * Set Authorization token to Request
 */
axios.interceptors.request.use(config => {
    if (authService.isLoggedIn()) {
        config.headers.common['Authorization'] = 'Bearer ' + authService.getToken()
    }

    return config
});

/**
 * Roles page checker
 */
router.beforeEach((to, from, next) => {
    if (to.meta.hasOwnProperty('roles') && authService.isLoggedIn()) {
        if (!to.meta.roles.includes(authService.getRoleUserRole().name)) {
            next({
                path: '/login',
            })
        } else {
            next()
        }
    } else {
        next()
    }
});

/**
 * Guarded page checker
 */
router.beforeEach((to, from, next) => {
    if (to.meta.hasOwnProperty('guarded') && !authService.isLoggedIn()) {
        next({
            path: '/login',
        })
    } else {
        next()
    }
});

router.beforeEach((to, from, next) => {
    const required = ['email', 'last_name', 'first_name', 'phone']

    if (!authService.isLoggedIn()) {
        next()
    }


    axios.post('/api/auth/me')
        .then(response => {
            if (response === undefined) {
                next()
            }

            let user = response.data

            let requiredStatus = true

            required.forEach(item => {
                if (!user[item]) {
                    requiredStatus = false
                }
            })


            if (
                to.name !== 'Profile'
                && authService.isLoggedIn()
                && (user.hasOwnProperty('roles') && user.roles[0].name !== 'ROLE_ADMIN')
                && !requiredStatus
            ) {
                app.$toastr('warning', 'Please, provide full information')

                next({
                    path: '/profile',
                    meta: {
                        flash: true
                    }
                })
            } else {
                next()
            }
        })
})

router.afterEach((to, from, next) => {
    if($(window).width()<999){
        $('.close_menu').remove();
        $( ".profile-usermenu" ).animate({
            left: "-100%",
        }, 600, function() {
            // Animation complete.
            $('.profile-usermenu').removeClass('active');
        });
    }
    else{
        $( ".profile-usermenu" ).animate({
            left: "-19.666%",
        }, 800, function() {
            // Animation complete.
            $('.profile-usermenu').removeClass('active');
        });
    }
})

/**
 * @TODO: Token Refresh
 */
axios.interceptors.response.use(null, err => {
    let res = err.response
    if (
        res.status === 401 &&
        (res.data.message && res.data.message === 'Unauthenticated.')
    ) {
        authService.logOut(false, () => {
            window.location = window.location.origin
        })
    }
});

const app = new Vue({
    el: '#app',
    store,
    router
});
