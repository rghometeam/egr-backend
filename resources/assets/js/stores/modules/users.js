import * as types from '../mutation-types'
// import { Questionnaire } from '../../services/api'
import Vue from 'vue'
import axios from 'axios'

const state = {
    user: {},
}

const getters = {
    user: state => state.user
}

const actions = {
    getUser ({ commit, getters }) {
        try {
            axios.post('/api/auth/me')
                .then(response => {
                    commit(types.RECEIVE_USER, response.data)
                })
        } catch (e) {
            console.error(e)
        }
    },
}

const mutations = {
    [types.RECEIVE_USER] (state, data) {
        state.user = data
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}