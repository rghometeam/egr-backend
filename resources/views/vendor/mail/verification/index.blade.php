@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot


<h1 style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#2f3133;font-size:19px;font-weight:bold;margin-top:0;text-align:left">{{$text['title'] }}</h1>





    {{-- Body --}}
    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
        {{ $text['text'] }}
    </p>

    @if(isset($text['button_text']))
        @component('mail::button', ['url' => $url])
            {{ $text['button_text'] }}
        @endcomponent
    @endif


    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
        Regards, <br>
        {{ config('app.name') }}
    </p>

    {{-- Subcopy --}}

    @if(isset($text['button_text']))
        @slot('subcopy')
            @component('mail::subcopy')
                <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;line-height:1.5em;margin-top:0;text-align:left;font-size:12px">
                    If you’re having trouble clicking the {{$text['button_text']}} button, copy and paste the URL below into your web browser:
                    {{$url}}
                </p>
            @endcomponent
        @endslot
    @endif


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
        @endcomponent
    @endslot
@endcomponent