<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BitCollege</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer></script>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">

    <style>
        .container-fluid {
            max-width: 1920px;
            width: 100%;
            padding-top: 50px;
        }

        .hr {
            clear: both;
            margin-top: 20px;
        }

        .browserupgrade {
            margin: 0.2em 0;
            /*background: #ccc;*/
            color: #000;
            padding: 120px 0;
            font-size: 20px;
            text-align: center;
        }
    </style>

</head>
<body>

<div class="container-fluid">
    <div id="app">
        <div class="row">
            <div class="col-md-12">
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please use <a href="https://www.mozilla.org/">Firefox</a> or <a href="https://www.google.ru/chrome/index.html">Chrome</a>.</p>
            </div>
        </div>

    </div>
</div>

</body>
</html>