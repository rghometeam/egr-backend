# ************************************************************
# Sequel Pro SQL dump
# Версия 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Адрес: 127.0.0.1 (MySQL 5.7.20)
# Схема: some-tg-bot
# Время создания: 2018-02-07 18:17:47 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы entities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entities`;

CREATE TABLE `entities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('button','link') COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `welcome` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `entities_parent_id_foreign` (`parent_id`),
  CONSTRAINT `entities_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `entities` WRITE;
/*!40000 ALTER TABLE `entities` DISABLE KEYS */;

INSERT INTO `entities` (`id`, `title`, `link`, `type`, `parent_id`, `created_at`, `updated_at`, `welcome`)
VALUES
	(7,'? BUY AND SELL ?',NULL,'button',NULL,'2018-02-07 16:00:44','2018-02-07 16:58:15','??? Some text BUY AND SELL???'),
	(8,'? DISCUSSIONS ?',NULL,'button',NULL,'2018-02-07 16:04:26','2018-02-07 16:58:38','??? Some text CHAT AND DISCUSSION???'),
	(9,'?SIGNALS ?',NULL,'button',NULL,'2018-02-07 16:05:56','2018-02-07 16:58:50','???Some Text SIGNALS???'),
	(10,'? WEBSITE ?','https://www.coindesk.com/','link',NULL,'2018-02-07 16:07:14','2018-02-07 16:07:14',NULL),
	(11,'? LocalBitcoins','https://localbitcoins.com/','link',7,'2018-02-07 16:51:06','2018-02-07 16:51:06',NULL),
	(12,'?Exmo','https://exmo.com/','link',7,'2018-02-07 16:51:54','2018-02-07 16:52:06',NULL),
	(13,'☄️ Kuna','https://kuna.io/','link',7,'2018-02-07 16:52:47','2018-02-07 16:52:47',NULL),
	(14,'? Telegram','https://telegram.org/','link',8,'2018-02-07 16:54:39','2018-02-07 16:54:39',NULL),
	(15,'? Whatsapp','https://www.whatsapp.com/?l=uk','link',8,'2018-02-07 16:55:15','2018-02-07 16:55:15',NULL),
	(16,'? Viber','https://www.viber.com/','link',8,'2018-02-07 16:55:43','2018-02-07 16:55:43',NULL),
	(17,'☀️GMaps','https://www.google.com.ua/maps','link',9,'2018-02-07 16:56:21','2018-02-07 16:57:25',NULL),
	(18,'? GEarth','https://www.google.com/intl/uk/earth/','link',9,'2018-02-07 16:57:12','2018-02-07 16:57:12',NULL);

/*!40000 ALTER TABLE `entities` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(4,'2018_02_07_122632_create_entities_table',2),
	(5,'2018_02_07_142950_add_welcome_message',3);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Дамп таблицы users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
