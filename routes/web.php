<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/browser', 'HomeController@browser')->name('browser');

Route::get('/builder', 'HomeController@index')->name('home');
Route::get('/builder/create', 'HomeController@createGet')->name('create');
Route::get('/builder/edit/{id}', 'HomeController@editGet')->name('editGet');

Route::get('/users', 'HomeController@usersIndex')->name('usersIndex');
Route::get('/users/create', 'HomeController@userCreate')->name('userCreate');
Route::get('/users/edit/{id}', 'HomeController@userEdit')->name('userEdit');

Route::get('/referrals', 'HomeController@referralsIndex')->name('referralsIndex');
Route::get('/withdrawal', 'HomeController@withdrawalIndex')->name('withdrawalIndex');

Route::get('/transactions', 'HomeController@transactionsIndex')->name('transactionsIndex');
Route::get('/transactions/edit/{id}', 'HomeController@transactionsEdit')->name('transactionsEdit');

Route::get('/profile', 'HomeController@profileIndex')->name('profileIndex');

Route::get('/logs', 'HomeController@logsIndex')->name('logsIndex');
Route::get('/faq-user', 'HomeController@faqUserIndex')->name('faqUserIndex');
Route::get('/faq-admin', 'HomeController@faqAdminIndex')->name('faqAdminIndex');
Route::get('/faq-admin/create', 'HomeController@faqAdminCreate')->name('faqAdminCreate');
Route::get('/faq-admin/edit/{id}', 'HomeController@faqAdminEdit')->name('faqAdminEdit');

Route::get('/settings', 'HomeController@settingsIndex')->name('settingsIndex');
Route::get('/settings/edit/{id}', 'HomeController@settingsCreate')->name('settingsCreate');

Route::get('/email-details', 'HomeController@emailDetailsIndex')->name('emailDetailsIndex');
Route::get('/email-details/edit/{id}', 'HomeController@emailDetailEdit')->name('emailDetailEdit');

Route::get('/packages', 'HomeController@packagesIndex')->name('packagesIndex');
Route::get('/packages/create', 'HomeController@packageCreate')->name('packageCreate');
Route::get('/packages/edit/{id}', 'HomeController@packageEdit')->name('packageEdit');

Route::get('/black-list', 'HomeController@blackListIndex')->name('blackListIndex');
Route::get('/black-list/create', 'HomeController@blackListCreate')->name('blackListCreate');
Route::get('/black-list/edit/{id}', 'HomeController@blackListEdit')->name('blackListEdit');

Route::get('/dashboard', 'HomeController@dashboardIndex')->name('dashboardIndex');

Route::get('/confirmation/{id}/{token}', 'HomeController@confirm');
Route::get('/reset', 'HomeController@reset');
Route::get('/new-password/{id}/{token}', 'HomeController@newPassword');

Route::get('/addresses', 'HomeController@addressesIndex')->name('addressesIndex');
Route::get('/addresses/create', 'HomeController@addressesCreate')->name('addressesCreate');
Route::get('/addresses/edit/{id}', 'HomeController@addressesEdit')->name('addressesEdit');

Route::get('/telegram-auth', 'HomeController@telegramAuthIndex')->name('telegramAuthIndex');
Route::get('/telegram-import', 'HomeController@telegramImportIndex')->name('telegramImportIndex');

Route::get('/login', 'HomeController@loginGet')->name('loginGet');
Route::get('/login-old', 'HomeController@loginOldGet')->name('loginOldGet');
Route::get('/register', 'HomeController@registerGet')->name('registerGet');

Route::match(['get', 'post'], '/botman', 'BotManController@handle');
