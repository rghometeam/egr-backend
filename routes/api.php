<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('login-old', 'AuthController@loginOld');
    Route::post('change-phone', 'AuthController@changePhone');
    Route::post('check-pin', 'AuthController@checkPin');
    Route::post('confirmation', 'AuthController@confirmation');
    Route::post('new-password', 'AuthController@newPassword');
    Route::post('reset', 'AuthController@reset');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('check-username', 'AuthController@checkUsername');
});

Route::get('/roles', 'RoleControllerController@getRoles')->name('getRoles');

Route::group([
    'prefix' => 'proto'
], function ($router) {
    Route::post('/send-phone', 'TelegramMTProtoAuthController@sendPhone')->name('sendPhone');
    Route::post('/send-code', 'TelegramMTProtoAuthController@sendCode')->name('sendCode');
    Route::post('/send-password', 'TelegramMTProtoAuthController@sendPassword')->name('sendPassword');
    Route::post('/send-group', 'TelegramMTProtoImportController@sendGroup')->name('sendGroup');
    Route::post('/import-group', 'TelegramMTProtoImportController@importGroupMembers')->name('importGroupMembers');
    Route::get('/get-statistic', 'TelegramMTProtoImportController@getBulkMessagesStatistic')->name('getBulkMessagesStatistic');
    Route::get('/get-messages', 'TelegramMTProtoImportController@getMessages')->name('getMessages');
    Route::post('/save-messages', 'TelegramMTProtoImportController@saveMessages')->name('saveMessages');
    Route::get('/get-meta', 'TelegramMTProtoImportController@getMeta')->name('getMeta');
    Route::post('/save-meta', 'TelegramMTProtoImportController@saveMeta')->name('saveMeta');
});


Route::get('/entities', 'ApiController@getAllEntities')->name('getAllEntities');
Route::get('/entities-button', 'ApiController@getButtonEntities')->name('getButtonEntities');
Route::post('/entity', 'ApiController@createEntity')->name('createEntity');
Route::put('/entity', 'ApiController@updateEntity')->name('updateEntity');
Route::get('/entity/{id}', 'ApiController@getEntity')->name('getEntity');
Route::get('/entity-delete/{id}', 'ApiController@deleteEntity')->name('deleteEntity');


Route::get('/admins', 'UserController@getAdmins')->name('getAdmins');

Route::post('/user', 'UserController@createUser')->name('createUser');
Route::post('/user/{id}', 'UserController@updateUser')->name('updateUser');
Route::get('/users', 'UserController@getUsers')->name('getUsers');
Route::post('/users-filter', 'UserController@filter')->name('filterUsers');
Route::get('/user/{id}', 'UserController@getUser')->name('getUser');
Route::delete('/user-delete/{id}', 'UserController@deleteUser')->name('deleteUser');
Route::get('/profile', 'UserController@profile')->name('profile');
Route::get('/balance', 'UserController@balance')->name('balance');
Route::post('/update-profile', 'UserController@updateProfile')->name('updateProfile');

Route::post('/package', 'PackageController@createPackage')->name('createPackage');
Route::post('/package/{id}', 'PackageController@updatePackage')->name('updatePackage');
Route::get('/packages', 'PackageController@getPackages')->name('getPackages');
Route::get('/package/{id}', 'PackageController@getPackage')->name('getPackage');
Route::delete('/package-delete/{id}', 'PackageController@deletePackage')->name('deletePackage');

Route::get('/packages-for-admin', 'PackageController@getPackagesForAdmin')->name('getPackagesForAdmin');

Route::post('/setting/{id}', 'SettingController@updateSetting')->name('updateSetting');
Route::get('/settings', 'SettingController@getSettings')->name('getSettings');
Route::get('/setting/{id}', 'SettingController@getSetting')->name('getSetting');
Route::post('/cryptogram-sync', 'SettingController@cryptogramSync')->name('cryptogramSync');

Route::get('/settings-for-admin', 'SettingController@getSettingsForAdmin')->name('getSettingsForAdmin');

Route::post('/email-detail/{id}', 'EmailDetailController@updateEmailDetail')->name('updateEmailDetail');
Route::get('/email-details', 'EmailDetailController@getEmailDetails')->name('getEmailDetails');
Route::get('/email-detail/{id}', 'EmailDetailController@getEmailDetail')->name('getEmailDetail');

Route::post('/transaction', 'TransactionController@createTransaction')->name('createPackage');
Route::post('/transaction/{id}', 'TransactionController@updateTransaction')->name('updatePackage');
Route::get('/transactions', 'TransactionController@getTransactions')->name('getTransactions');
Route::post('/transactions-filter', 'TransactionController@filter')->name('filterTransactions');
Route::get('/transaction/{id}', 'TransactionController@getTransaction')->name('getPackage');
Route::delete('/transaction-delete/{id}', 'TransactionController@deleteTransaction')->name('deletePackage');

//Route::get('/transactions-for-admin', 'TransactionController@getTransactionsForAdmin')->name('getTransactionsForAdmin');

Route::post('/withdrawal', 'WithdrawalController@create')->name('createWithdrawal');
Route::get('/withdrawals', 'WithdrawalController@all')->name('allWithdrawal');

Route::get('/transaction-check/{id}', 'TransactionController@checkTransaction')->name('checkTransaction');
Route::get('/transaction-latest', 'TransactionController@latestTransaction')->name('latestTransaction');

Route::post('/log', 'LogController@createLog')->name('createLog');
Route::post('/log/{id}', 'LogController@updateLog')->name('updateLog');
Route::get('/logs', 'LogController@getLogs')->name('getLogs');
Route::get('/log/{id}', 'LogController@getLog')->name('getLog');
Route::delete('/log-delete/{id}', 'LogController@deleteLog')->name('deleteLog');

Route::post('/faq', 'FaqController@createFaq')->name('createFaq');
Route::post('/faq/{id}', 'FaqController@updateFaq')->name('updateFaq');
Route::get('/faqs', 'FaqController@getFaqs')->name('getFaqs');
Route::get('/faq/{id}', 'FaqController@getFaq')->name('getFaq');
Route::delete('/faq-delete/{id}', 'FaqController@deleteFaq')->name('deleteFaq');

Route::post('/black-list', 'BlackListController@createBlackList')->name('createBlackList');
Route::post('/black-list/{id}', 'BlackListController@updateBlackList')->name('updateBlackList');
Route::get('/black-lists', 'BlackListController@getBlackLists')->name('getBlackLists');
Route::get('/black-list/{id}', 'BlackListController@getBlackList')->name('getBlackList');
Route::delete('/black-list-delete/{id}', 'BlackListController@deleteBlackList')->name('deleteBlackList');

Route::post('/address', 'AddressController@createAddress')->name('createAddress');
Route::post('/address/{id}', 'AddressController@updateAddress')->name('updateAddress');
Route::get('/addresses', 'AddressController@getAddresses')->name('getAddresses');
Route::get('/address/{id}', 'AddressController@getAddress')->name('getAddress');
Route::delete('/address-delete/{id}', 'AddressController@deleteAddress')->name('deleteAddress');

Route::post('/address-for-user', 'AddressController@getAddressForUser')->name('getAddressForUser');

Route::post('/referrals/track', 'ReferralsController@track')->name('track');
Route::get('/referrals/statistic', 'ReferralsController@statistic')->name('statistic');
Route::get('/referrals/transactions', 'ReferralsController@transactions')->name('transactions');


Route::post('/signal', 'SignalController@createSignal')->name('createSignal');
Route::post('/signal/{id}', 'SignalController@updateSignal')->name('updateSignal');
Route::get('/signals/{status}', 'SignalController@getSignalsByStatus')->name('getSignalsByStatus');
Route::get('/signal/{id}', 'SignalController@getSignal')->name('getSignal');
Route::delete('/signal-delete/{id}', 'SignalController@deleteSignal')->name('deleteSignal');

Route::post('/owner', 'OwnerController@createOwner')->name('createOwner');
Route::post('/owner/{id}', 'OwnerController@updateOwner')->name('updateOwner');
Route::get('/owners', 'OwnerController@getOwners')->name('getOwners');
Route::get('/owner/{id}', 'OwnerController@getOwner')->name('getOwner');
Route::delete('/owner-delete/{id}', 'OwnerController@deleteOwner')->name('deleteOwner');



