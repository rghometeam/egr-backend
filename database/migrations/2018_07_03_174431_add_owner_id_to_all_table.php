<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOwnerIdToAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });

        Schema::table('black_lists', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('black_lists', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });

        Schema::table('email_details', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('email_details', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });

        Schema::table('faqs', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('faqs', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });

        Schema::table('logs', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('logs', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });

        Schema::table('packages', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('packages', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });

        Schema::table('referral_tracks', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('referral_tracks', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });

        Schema::table('signals', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('signals', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });

        Schema::table('targets', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('targets', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });

        Schema::table('withdrawals', function (Blueprint $table) {
            $table->unsignedInteger('owner_id')->index()->nullable();
        });

        Schema::table('withdrawals', function (Blueprint $table) {
            $table->foreign('owner_id')
                ->references('id')
                ->on('owners')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
