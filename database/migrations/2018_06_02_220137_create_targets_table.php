<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('targets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('signal_id')->index();
            $table->integer('number');
            $table->decimal('value',15,8);
            $table->boolean('is_hit')->default(false);
            $table->timestamps();
        });

        Schema::table('targets', function (Blueprint $table) {
            $table->foreign('signal_id')
                ->references('id')
                ->on('signals')
                ->onUpdate('cascade')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('targets');
    }
}
