<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAddressIdToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
//            $table->dropForeign('transactions_address_id_foreign');
//            $table->dropIndex('transactions_address_id_index');
//            $table->dropColumn('address_id');

            \App\Models\Transaction::truncate();
        });

        Schema::table('transactions', function (Blueprint $table) {


            $table->unsignedInteger('address_id')->index();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('address_id')
                ->references('id')
                ->on('addresses')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
