<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('entities');
        Schema::create('entities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('link')->nullable();
            $table->enum('type', ['button', 'link']);
            $table->unsignedInteger('parent_id')->nullable();
            $table->timestamps();
        });

        Schema::table('entities', function (Blueprint $table) {
            $table
                ->foreign('parent_id')
                ->references('id')
                ->on('entities')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entities');
    }
}
