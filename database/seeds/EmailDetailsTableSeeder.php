<?php

use Illuminate\Database\Seeder;

class EmailDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        DB::table('email_details')->insert([
            'text' => 'Some Text',
            'title' => 'Some title',
            'button_text' => 'button_text',
            'type' => 'email_confirmation',
            'subject' => 'Subject'
        ]);

        DB::table('email_details')->insert([
            'text' => 'Some Text',
            'title' => 'Some title',
            'button_text' => 'button_text',
            'type' => 'password_reset',
            'subject' => 'Subject'
        ]);


        DB::table('email_details')->insert([
            'text' => 'Some Text',
            'title' => 'Some title',
            'type' => 'subscription_ends',
            'subject' => 'Subject'
        ]);

    }
}
