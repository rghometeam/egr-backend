<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['ROLE_ADMIN', 'ROLE_USER', 'ROLE_PREMIUM', ];
        foreach ($roles as $role) {
            \App\Role::create(['name' => $role, 'display_name' => $role]);
        }

        \App\Role::create(['name' => 'ROLE_FREE', 'display_name' => 'ROLE_FREE']);
    }
}
