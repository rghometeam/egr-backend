<?php

use Illuminate\Database\Seeder;

class EditorRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create(['name' => 'ROLE_EDITOR', 'display_name' => 'ROLE_EDITOR']);
    }
}
