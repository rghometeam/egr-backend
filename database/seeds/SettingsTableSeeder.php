<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('settings')->insert([
//            'name' => 'btc_wallet',
//            'value' => 'test wallet'
//        ]);
//
//        DB::table('settings')->insert([
//            'name' => 'eth_wallet',
//            'value' => 'test wallet'
//        ]);
//
       DB::table('settings')->insert([
           'name' => 'btc_fee',
           'value' => '0.001'
       ]);

       DB::table('settings')->insert([
           'name' => 'eth_fee',
           'value' => '0.03'
       ]);

//        DB::table('settings')->insert([
//            'name' => 'subscribe_days',
//            'value' => '30'
//        ]);

//        DB::table('settings')->insert([
//            'name' => 'channel_id',
//            'value' => '1289952952'
//        ]);
//
//        DB::table('settings')->insert([
//            'name' => 'app_id',
//            'value' => '1289952952'
//        ]);
//
//        DB::table('settings')->insert([
//            'name' => 'app_secret',
//            'value' => '1289952952'
//        ]);

        DB::table('settings')->insert([
            'name' => 'email_confirmation_text',
            'value' => 'To validate your email click on the button below'
        ]);

        DB::table('settings')->insert([
            'name' => 'btc_max_fee',
            'value' => '0.001'
        ]);

        DB::table('settings')->insert([
            'name' => 'eth_max_fee',
            'value' => '0.03'
        ]);

        DB::table('settings')->insert([
            'name' => 'email_name',
            'value' => 'EasyGram'
        ]);
    }
}
