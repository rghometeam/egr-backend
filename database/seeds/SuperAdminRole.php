<?php

use Illuminate\Database\Seeder;

class SuperAdminRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create(['name' => 'ROLE_SUPER_ADMIN', 'display_name' => 'ROLE_SUPER_ADMIN']);
    }
}
