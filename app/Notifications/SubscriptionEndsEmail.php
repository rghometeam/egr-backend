<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 26.02.2018
 * Time: 13:55
 */

namespace App\Notifications;


use App\Models\EmailDetail;
use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SubscriptionEndsEmail extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /**
         * var Setting $name
         */
        $name = Setting::where('name', 'email_name')->first();
        /**
         * var EmailDetail $text
         */
        $text = EmailDetail::where('type', 'subscription_ends')->first();

        return (new MailMessage)
            ->subject($text['subject'])
            ->from(env("MAIL_USERNAME"), $name['value'])
            ->markdown('vendor.mail.verification.index', ['text' => $text] )
        ;
    }
}