<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 26.02.2018
 * Time: 18:50
 */

namespace App\Notifications;

use App\Models\EmailDetail;
use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class PasswordReset
 *
 * @package App\Notifications
 */
class PasswordReset extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var PasswordReset
     */
    protected $passwordReset;

    /**
     * PasswordReset constructor.
     * @param \App\Models\PasswordReset $passwordReset
     */
    public function __construct(\App\Models\PasswordReset $passwordReset)
    {
        $this->passwordReset = $passwordReset;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        /**
         * var Setting $name
         */
        $name = Setting::where('name', 'email_name')->first();
        /**
         * var EmailDetail $text
         */
        $text = EmailDetail::where('type', 'password_reset')->first();

        return (new MailMessage)
            ->subject($text['subject'])
            ->from(env("MAIL_USERNAME"), $name['value'])
            ->markdown('vendor.mail.verification.index', ['text' => $text, 'url' => url("new-password/$notifiable->id/" . $this->passwordReset->token)] )
            ;

//        return (new MailMessage)
//            ->from(env('MAIL_USERNAME'), env('APP_NAME'))
//            ->subject('Password Reset')
//            ->line('Password Reset')
//            ->line('To reset your password click on the button below')
//            ->action('Password Reset', url("new-password/$notifiable->id/" . $this->passwordReset->token))
//        ;
    }
}