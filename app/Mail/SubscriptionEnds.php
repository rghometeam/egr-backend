<?php

namespace App\Mail;

use App\Repositories\SettingRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionEnds extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var SettingRepository
     */
    protected $settingRepository;

    /**
     * SubscriptionEnds constructor.
     * @param SettingRepository $settingRepository
     */
    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('example@example.com')
            ->view('email.subscription-ends')
            ->with([

            ])
        ;
    }
}
