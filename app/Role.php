<?php namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;
    const ROLE_PREMIUM = 3;
    const ROLE_FREE = 4;
    const ROLE_EDITOR = 5;
    const ROLE_SUPER_ADMIN = 6;

    protected $fillable = [
        'name', 'display_name', 'description', 'created_at', 'updated_at'
    ];
}
