<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 31.03.2018
 * Time: 14:21
 */

namespace App\Exceptions;

use Illuminate\Http\Request;

trait RestTrait
{

    /**
     * Determines if request is an api call.
     *
     * If the request URI contains '/api/v'.
     *
     * @param Request $request
     * @return bool
     */
    protected function isApiCall(Request $request)
    {
        return strpos($request->getUri(), '/api/') !== false;
    }

}