<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'Price_BTC', 'Price_ETH', 'days', 'owner_id'
    ];

    /**
     * @var array
     */
    protected $with = [
        'owner.profile'
    ];

    public function transactions()
    {
        return $this->hasMany('App\Transaction','package_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\Owner','owner_id');
    }
}
