<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Signal
 * @package App\Models
 */
class Signal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'market', 'pair', 'buy_zone_one', 'buy_zone_two',
        'buy_zone_flag', 'stop_loss', 'status', 'link',
        'reply_id', 'term', 'owner_id'
    ];

    /**
     * @var array
     */
    protected $with = [
        'owner.profile'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function targets()
    {
        return $this->hasMany('App\Models\Target','signal_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\Owner','owner_id');
    }
}
