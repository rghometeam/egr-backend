<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    protected $fillable = [
        'title', 'type', 'link', 'parent_id', 'welcome', 'image'
    ];

    public function parent()
    {
        return $this->belongsTo(Entity::class, 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(Entity::class, 'parent_id', 'id');
    }
}
