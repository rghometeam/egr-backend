<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'host', 'user_id', 'channel_premium', 'channel_log'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User','owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany('App\Models\Address','owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blackLists()
    {
        return $this->hasMany('App\Models\BlackList','owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emailDetails()
    {
        return $this->hasMany('App\Models\EmailDetail','owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function packages()
    {
        return $this->hasMany('App\Models\Package','owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function settings()
    {
        return $this->hasMany('App\Models\Setting','owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function signals()
    {
        return $this->hasMany('App\Models\Signal','owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo('App\User', 'user_id');
    }



    protected static function boot() {
        parent::boot();

        static::deleting(function($owner) { // before delete() method call this
            $owner->users()->delete();
            $owner->addresses()->delete();
            $owner->packages()->delete();
            $owner->settings()->delete();
            $owner->signals()->delete();
            $owner->emailDetails()->delete();
            $owner->blackLists()->delete();
            $owner->profile()->delete();
            // do the rest of the cleanup...
        });
    }
}
