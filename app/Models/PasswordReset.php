<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 26.02.2018
 * Time: 18:09
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class PasswordReset
 * @package App\Models
 */
class PasswordReset extends Model
{
    /**
     * @var string
     */
    protected $table = 'password_resets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'token'
    ];

    public function setUpdatedAt($value) {  }
}