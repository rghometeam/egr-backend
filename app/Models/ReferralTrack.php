<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferralTrack
 * @package App\Models
 */
class ReferralTrack extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'referred_by', 'owner_id'
    ];

    /**
     * @var array
     */
    protected $with = [
        'referred'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function referred()
    {
        return $this->belongsTo('App\User','referred_by');
    }
}
