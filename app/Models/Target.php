<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'signal_id', 'number', 'value', 'is_hit', 'owner_id'
    ];

    public function signal()
    {
        return $this->belongsTo('App\Models\Signal','signal_id');
    }
}
