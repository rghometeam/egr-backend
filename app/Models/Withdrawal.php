<?php
/**
 * Created by PhpStorm.
 * User: rghome
 * Date: 4/28/18
 * Time: 01:09
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Withdrawal
 * @package App\Models
 */
class Withdrawal extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'from_user_id', 'to_user_id', 'amount', 'owner_id'
    ];

    /**
     * @var array
     */
    protected $with = [
        'fromUser', 'fromUser.roles', 'toUser', 'toUser.roles', 'owner'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fromUser()
    {
        return $this->belongsTo('App\User','from_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function toUser()
    {
        return $this->belongsTo('App\User','to_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\Owner','owner_id');
    }
}