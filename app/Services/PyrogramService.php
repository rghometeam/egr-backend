<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 19.04.2018
 * Time: 01:44
 */

namespace App\Services;


use App\Models\Owner;
use App\Models\Signal;
use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * Class PyrogramService
 * @package App\Services
 */
class PyrogramService
{
    /**
     * @var string
     */
    private $_baseUrl = 'http://localhost:82';

    /**
     * @var Client
     */
    private $_client;

    /**
     * PyrogramService constructor.
     */
    public function __construct()
    {
        $this->_client = new Client([
            'base_uri' => $this->_baseUrl
        ]);
    }

    /**
     * @param $message
     * @return mixed
     */
    public function publishToLogsChannel($message)
    {
        /**
         * @var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('id', $user['owner_id'])->first();

        $logChannel = $owner['channel_log'];

        $response = $this->_client->post('/log/send', [
            RequestOptions::JSON => [
                'message' => $message,
                'channel' => $logChannel
            ]
        ]);

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function sendCode(User $user)
    {
        $response = $this->_client->post('/send-code', [
            RequestOptions::JSON => [
                'user' => $user,
            ]
        ]);

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function inviteToChannel(User $user)
    {
        /**
         * @var Owner $owner
         */
        $owner = Owner::where('id', $user['owner_id'])->first();

        $premiumChannel = $owner['channel_premium'];

        $response = $this->_client->post('/channel/invite', [
            RequestOptions::JSON => [
                'user' => $user,
                'channel' => $premiumChannel
            ]
        ]);

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function banInChannel(User $user)
    {
        /**
         * @var Owner $owner
         */
        $owner = Owner::where('id', $user['owner_id'])->first();

        $premiumChannel = $owner['channel_premium'];

        $response = $this->_client->post('/channel/ban', [
            RequestOptions::JSON => [
                'user' => $user,
                'channel' => $premiumChannel
            ]
        ]);

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Signal $signal
     * @return mixed
     */
    public function cancelSignal(Signal $signal)
    {
        $pair = explode('BTC', $signal->pair);

        $message = $signal->market . PHP_EOL;
        $message .= '#' . $pair[0] . '/BTC️ Closed ⚠️' . PHP_EOL;

        $response = $this->_client->post('/signals/reply', [
            RequestOptions::JSON => [
                'message' => $message,
                'reply_id' => $signal->reply_id
            ]
        ]);

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Signal $signal
     * @return mixed
     */
    public function publishSignal(Signal $signal)
    {
        $response = $this->_client->post('/signals/publish', [
            RequestOptions::JSON => [
                'message' => $this->buildSignal($signal),
                'pair' => $signal->pair
            ]
        ]);

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Signal $signal
     * @return mixed
     */
    public function updateSignal(Signal $signal)
    {
        $response = $this->_client->post('/signals/edit', [
            RequestOptions::JSON => [
                'message' => $this->buildSignal($signal),
                'message_id' => $signal->reply_id,
                'pair' => $signal->pair
            ]
        ]);

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Signal $signal
     * @return string
     */
    private function buildSignal(Signal $signal)
    {
        $pair = explode('BTC', $signal->pair);

        $message = '⚡️⚡️ #' . $pair[0] . ' - BTC ⚡️⚡️' . PHP_EOL;
        $message .= 'Market: ' . $signal->market . PHP_EOL;
        $message .= '**Current Price: {{cp}}**' . PHP_EOL;
        $message .= 'Buy Zone: ' . $signal->buy_zone_one . ' - ' . $signal->buy_zone_two . PHP_EOL;
        $message .= 'Targets: ' . PHP_EOL;

        foreach ($signal->targets as $target) {
            $message .= $target->number . ') ' . $target->value . PHP_EOL;
        }

        $message .= 'Stop - loss: ' . $signal->stop_loss . PHP_EOL;

        if ($signal->link) {
            $message .= PHP_EOL . 'Link: ' . $signal->link . PHP_EOL;
        }

        return $message;
    }
}