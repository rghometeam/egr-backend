<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 23.02.2018
 * Time: 16:44
 */

namespace App\Services;


use App\Models\Withdrawal;
use App\Repositories\LogRepository;
use App\User;
use danog\MadelineProto\Exception;
use danog\MadelineProto\Serialization;

/**
 * Class MTProtoService
 * @package App\Services
 */
class MTProtoService
{
    /**
     * @var \danog\MadelineProto\API
     */
    protected $proto;

    /**
     * @var string
     */
    protected $sessionName = 'store.bot';

    /**
     * @var array
     */
    protected $settings = [
        'app_info' => [
            'api_id' => 156890,
            'api_hash' => '3d1d131b65457f488945bbba2cbe9666'
        ],
        'logger' => [
            'logger' => 0
        ]
    ];

    protected $botToken = '522900701:AAFIciKu94pTeJxaNfgyNxoGcachufB7cZ8';

    /**
     * @var string
     */
    protected $inputChannel = 1341896184;

    /**
     * MTProtoService constructor.
     */
    public function __construct()
    {
//        try {
//            $this->proto = unserialize(file_get_contents(storage_path() . '/store.bot'));
//        } catch (\Exception $exception) {
//            $this->proto = new \danog\MadelineProto\API($this->settings);
//
//            $this->proto->bot_login($this->botToken);
//
//            $this->serialize();
//        }
    }

    /**
     * @param Withdrawal $withdrawal
     * @return \danog\MadelineProto\updates
     */
    public function notifyAboutWithdrawal(Withdrawal $withdrawal)
    {
        $message = [
            'Withdrawal:',
            'From: @' . $withdrawal->fromUser->telegram_name,
            'To User: @' . $withdrawal->toUser->telegram_name,
            'Amount: ' . $withdrawal->amount,
            'Created At: ' . $withdrawal->created_at
        ];

        $message = implode(PHP_EOL, $message);

        $update = $this->proto->messages->sendMessage([
            'peer' => 'channel#' . $this->inputChannel,
            'message' => $message
        ]);

        $this->serialize();

        return $update;
    }

    /**
     * @param $phone
     * @return mixed
     */
    public function sentPhone($phone)
    {
        $status = $this->proto->phone_login($phone);

        $this->serialize();

        return $status;
    }

    /**
     * @param User $user
     * @return \danog\MadelineProto\updates
     */
    public function inviteToChannel(User $user)
    {

//            $this->updatePeer();

        $update = $this->proto->messages->sendMessage([
            'peer' => 'channel#' . $this->inputChannel,
            'message' => 'Member @' . $user->telegram_name . ' Need to be Added. New Role:  ' . $user->roles[0]->display_name
        ]);

//        $this->updatePeer();

        try {
//            $a = $this->proto->channels->editBanned([
//                'channel' => 'channel#' . $this->inputChannel,
//                'user_id' => '@' . $user->telegram_name,
//                'banned_rights' => [
//                    '_' => 'channelBannedRights',
//                    'view_messages' => false,
//                    'send_messages' => false,
//                    'send_gifs' => false,
//                    'send_media' => false,
//                    'send_stickers' => false,
//                    'send_games' => false,
//                    'send_inline' => false,
//                    'embed_links' => false,
//                    'until_date' => 2147483647
//                ]
//            ]);
//
//            $b = $this->proto->channels->deleteUserHistory([
//                'user_id' => '@' . $user->telegram_name,
//                'channel' => 'channel#' . $this->inputChannel
//            ]);
        } catch (\Exception $exception) { }

//        $inviteUpdate = $this->proto->channels->exportInvite([
//            'channel' => 'channel#' . $this->inputChannel
//        ]);
//
//        $this->serialize();
//
//
//        sleep(10);
//        $status = $this->proto->messages->sendMessage([
//            'peer' => '@' . $user->telegram_name,
//            'message' => 'Hi, your invite link: ' . $inviteUpdate['link']
//        ]);

//        $this->serialize();



//        fwrite(fopen('/tmp/dump', 'w'), print_r([
//            $this->proto->get_info('@MaksVh'),
//        ], 1));

//        $testUser = $this->proto->get_info('@' . $user->telegram_name);
//
//        $status = $this->proto->contacts->importContacts(['contacts' => [
//            ['_' => 'inputPhoneContact', 'client_id' => $testUser['User']['id'], 'phone' => $testUser['User']['phone'], 'first_name' => $testUser['User']['first_name'], 'last_name' => (($testUser['User']['last_name']) ? $testUser['User']['last_name'] : 'Last Name')]
//        ]]);

//        fwrite(fopen('/tmp/dump', 'w'), print_r($status, 1));

//        $status = $this->proto->channels->inviteToChannel([
//            'channel' => 'channel#' . $this->inputChannel,
//            'users' => [
//                '@' . $user->telegram_name
//            ]
//        ]);

        $this->serialize();

        return $update;
    }

    /**
     * @param User $user
     * @return \danog\MadelineProto\updates
     */
    public function banInChannel(User $user)
    {

        $update = $this->proto->messages->sendMessage([
            'peer' => 'channel#' . $this->inputChannel,
            'message' => 'Member @' . $user->telegram_name . ' Need to be kicked. New Role:  ' . $user->roles[0]->display_name
        ]);

//        $this->updatePeer();

//        $status = $this->proto->channels->editBanned([
//            'channel' => 'channel#' . $this->inputChannel,
//            'user_id' => '@' . $user->telegram_name,
//            'banned_rights' => [
//                '_' => 'channelBannedRights',
//                'view_messages' => true,
//                'send_messages' => true,
//                'send_gifs' => true,
//                'send_media' => true,
//                'send_stickers' => true,
//                'send_games' => true,
//                'send_inline' => true,
//                'embed_links' => true,
//                'until_date' => 2147483647
//            ]
//        ]);

        $this->serialize();

        return $update;
    }

    /**
     * @param $code
     * @return mixed
     */
    public function sentCode($code)
    {
        $status = $this->proto->complete_phone_login(trim($code));

        $this->serialize();

        return $status;
    }

    /**
     * @param $password
     * @return mixed
     */
    public function sendPassword($password)
    {
        $status = $this->proto->complete_2fa_login($password);

        $this->serialize();

        return $status;
    }

    /**
     *
     */
    private function serialize()
    {
        $this->proto->session = $this->sessionName;
        file_put_contents(storage_path() . '/store.bot', serialize($this->proto));
    }

    /**
     * @param int $times
     * @return bool
     */
    private function updatePeer($times = 0)
    {
        try {
            $this->proto->get_pwr_chat($this->inputChannel);

            $this->serialize();

            return true;
        } catch (\Exception $exception) {
            if ($times > 10) {
                LogRepository::setError('telegram', $exception->getMessage());

                return false;
            }

            if ($exception->getMessage() === 'This peer is not present in the internal peer database') {
                sleep($times + 5);

                return $this->updatePeer(++$times);
            } else {

                LogRepository::setError('telegram', $exception->getMessage());
                return false;
            }
        }
    }
}