<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 23.02.2018
 * Time: 16:44
 */

namespace App\Services;

use App\Models\Setting;
use App\Models\Transaction;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

/**
 * Class CryptoCurrency
 * @package App\Services
 */
class CryptoCurrency
{
    /**
     * @var Client
     */
    private $guzzle;

    /**
     * CryptoCurrency constructor.
     */
    public function __construct()
    {
        $this->guzzle = new Client();

    }

    /**
     * @param Transaction $transaction
     * @return int
     */
    public function getStatus(Transaction $transaction)
    {
        $transactionData = $this->prepareInputData($transaction);

        return $this->compareData($transactionData);
    }

    /**
     * @param array $transaction
     * @return array|int
     */
    private function compareData(array $transaction)
    {
        $providerData = $this->getProviderData($transaction);

        $transaction['origin_price'] = (float) $transaction['price'];
        $transaction['price'] += (float) $transaction['fee'];

//        if (isset($providerData['error'])) {
//            return [
//                'status' => Transaction::STATUS_FAILED,
//                'message' => 'Provider error: ' . $providerData['error']['message']
//            ];
//        }
//
//        if (empty($providerData)) {
//            return [
//                'status' => Transaction::STATUS_PENDING,
//                'message' => 'Waiting service'
//            ];
//        }
//
//        if ($transaction['origin_address'] !== $providerData['origin_address']) {
//            return [
//                'status' => Transaction::STATUS_FAILED,
//                'message' => 'Addresses not equals (Origin: ' . $transaction['origin_address'] . '; Provider: ' . $providerData['origin_address'] . ')'
//            ];
//        }
//
//        if (($providerData['price'] >= ($transaction['origin_price'] - (float) $transaction['max_fee'])) && ($providerData['confirmations'] >= 2)) {
//            return [
//                'status' => Transaction::STATUS_SUCCESS,
//                'is_correct_fee' => false,
//                'message' => 'Transaction success, without fee (Origin: ' . $transaction['price'] . '; Provider: ' . $providerData['price'] . ')'
//            ];
//        }
//
//        if ($providerData['price'] < $transaction['price']) {
//            return [
//                'status' => Transaction::STATUS_FAILED,
//                'message' => 'Provider price less then transaction price + fee (Origin: ' . $transaction['price'] . '; Provider: ' . $providerData['price'] . ')'
//            ];
//        }
//
//        if ($providerData['confirmations'] < 0) {
//            return [
//                'status' => Transaction::STATUS_PENDING,
//                'message' => 'Less then 2 confirmation'
//            ];
//        }
//
//        if ($providerData['confirmations'] >= 2) {
//            return [
//                'status' => Transaction::STATUS_SUCCESS,
//                'is_correct_fee' => true,
//                'message' => 'Success'
//            ];
//        }

//        return [
//            'status' => Transaction::STATUS_SUCCESS,
//            'message' => 'Less then 2 confirmation'
//        ];
        //delete after demo
        return [
            'status' => Transaction::STATUS_SUCCESS,
            'message' => 'Success'
        ];
    }

    /**
     * @param array $transaction
     * @return array|bool|mixed
     */
    private function getProviderData(array $transaction)
    {
        if ($transaction['type'] === 'BTC') {
            return $this->processRequestBTC($transaction['tx_id'], $transaction['origin_address']);
        } elseif ($transaction['type'] === 'ETH') {
            return $this->processRequestETH($transaction['tx_id']);
        }
    }

    /**
     * @param Transaction $transaction
     * @return array
     */
    private function prepareInputData(Transaction $transaction)
    {
        $type = $transaction->type;
        $settingFee = Setting::where('name', mb_strtolower($type) . '_fee')->first();
//        $settingAddress = Setting::where('name', mb_strtolower($type) . '_wallet')->first();

        return [
            'type' => $type,
            'tx_id' => $transaction->tx_id,
            'price' => $transaction->price,
            'max_fee' => $transaction->max_fee,
            'fee' => $settingFee->value,
            'origin_address' => $transaction->address['value']
        ];
    }

    /**
     * @param $txid
     * @param $originWallet
     * @return array|bool
     */
    public function processRequestBTC($txid, $originWallet)
    {
        $url = 'https://blockexplorer.com/api/tx/';
        try {
            $response = $this->guzzle->get($url . $txid);

            $data = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

            $originAddress = null;
            $price = null;

            foreach ($data['vout'] as $block) {
                if (!isset($block['scriptPubKey']['addresses'])) continue;

                if ($block['scriptPubKey']['addresses'][0] === $originWallet) {
                    $originAddress = $block['scriptPubKey']['addresses'][0];
                    $price = (float) $block['value'];
                }
            }

            return [
                'tx_id' => $data['txid'],
                'confirmations' => $data['confirmations'],
                'fees' => $data['fees'],
                'price' => $price,
                'origin_address' => $originAddress,
            ];
        } catch (\Exception $exception) {
            return [];
        }
    }

    /**
     * @param string $txid
     * @return bool|mixed
     */
    public function processRequestETH($txid)
    {
        $url = 'https://api.ethplorer.io/getTxInfo/'. $txid .'?apiKey=freekey';

        try {
            $response = $this->guzzle->get($url);

            $data = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

            if (isset($data['error'])) {
                return $data;
            }

            return [
                'tx_id' => $data['hash'],
                'confirmations' => $data['confirmations'],
//                'fees' => $data['fees'],
                'price' => (float) $data['value'],
                'origin_address' => $data['to'],
            ];
        } catch (\Exception $exception) {
            return [];
        }
    }
}