<?php

namespace App\Console\Commands;

use App\Models\BlackList;
use App\Models\Transaction;
use App\Repositories\TransactionRepository;
use Illuminate\Console\Command;

/**
 * Class CheckBlackListTransactions
 * @package App\Console\Commands
 */
class CheckBlackListTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:black_list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var TransactionRepository
     */
    protected $transactionRepository;


    /**
     * CheckBlackListTransactions constructor.
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pendingTransactions = $this->transactionRepository->toBlackList();

        /** @var Transaction $transaction */
        foreach ($pendingTransactions as $transaction) {
            $blacklListItem = new BlackList();
            $blacklListItem->tx_id = $transaction->tx_id;
            $blacklListItem->owner_id = $transaction->owner_id;


            try {
                $blacklListItem->save();
            } catch (\Exception $exception) {}

        }
    }
}
