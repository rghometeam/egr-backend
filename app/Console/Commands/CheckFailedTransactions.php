<?php

namespace App\Console\Commands;

use App\Models\Transaction;
use App\Repositories\TransactionRepository;
use Illuminate\Console\Command;

class CheckFailedTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:failed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var TransactionRepository
     */
    protected $transactionRepository;


    /**
     * CheckBlackListTransactions constructor.
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pendingTransactions = $this->transactionRepository->failed();

        /** @var Transaction $transaction */
        foreach ($pendingTransactions as $transaction) {
            $transaction->status = Transaction::STATUS_PENDING;
            $transaction->save();

            $this->transactionRepository->processTransaction($transaction->id);
        }
    }
}
