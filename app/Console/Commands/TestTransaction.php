<?php
/**
 * Created by PhpStorm.
 * User: rghome
 * Date: 4/27/18
 * Time: 11:27
 */

namespace App\Console\Commands;

use App\Models\Transaction;
use App\Repositories\TransactionRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Console\Command;

class TestTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var TransactionRepository
     */
    protected $transactionRepository;

    /**
     * TestTransaction constructor.
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        parent::__construct();

        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transaction = Transaction::whereId(95)->first();
        $transaction->status = Transaction::STATUS_PENDING;
        $transaction->save();

        $this->transactionRepository->processTransaction($transaction->id);
    }
}