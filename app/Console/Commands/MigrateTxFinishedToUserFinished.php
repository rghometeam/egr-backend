<?php

namespace App\Console\Commands;

use App\Models\Transaction;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class MigrateTxFinishedToUserFinished extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:tx-to-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::has('transactions')->with('transactions')->get();

        foreach ($users as $user) {
            /** @var Collection $transactions */
            $transactions = $user->transactions;
            $actualTransaction = '';

            if ($transactions->count() > 1) {
                foreach ($transactions as $transaction) {
                    if ((int) $transaction->status === Transaction::STATUS_SUCCESS) {
                        $actualTransaction = $transaction;
                    }
                }
            } else {
                $actualTransaction = $transactions->first();
            }

            $user->finished_date = $actualTransaction->finished_date;
            $user->save();
        }
    }
}
