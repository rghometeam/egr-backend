<?php

namespace App\Console\Commands;

use App\Models\Transaction;
use App\Repositories\TransactionRepository;
use Illuminate\Console\Command;

/**
 * Class CheckPendingTransactions
 * @package App\Console\Commands
 */
class CheckPendingTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:pending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var TransactionRepository
     */
    protected $transactionRepository;

    /**
     * CheckPendingTransactions constructor.
     *
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pendingTransactions = $this->transactionRepository->pending();

        /** @var Transaction $transaction */
        foreach ($pendingTransactions as $transaction) {
            $this->transactionRepository->processTransaction($transaction->id);
        }
    }
}
