<?php

namespace App\Console\Commands;

use App\Mail\SubscriptionEnds;
use App\Models\Transaction;
use App\Repositories\SettingRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

/**
 * Class CheckEndingTransactions
 * @package App\Console\Commands
 */
class CheckEndingTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:ending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var TransactionRepository
     */
    protected $transactionRepository;

    /**
     * @var SettingRepository
     */
    protected $settingRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * CheckEndingTransactions constructor.
     * @param TransactionRepository $transactionRepository
     * @param UserRepository $userRepository
     * @param SettingRepository $settingRepository
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        UserRepository $userRepository,
        SettingRepository $settingRepository
    )
    {
        $this->settingRepository = $settingRepository;
        $this->transactionRepository = $transactionRepository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pendingTransactions = $this->userRepository->ending();

        /** @var User $user */
        foreach ($pendingTransactions as $user) {
            Mail::to($user)->send(new SubscriptionEnds($this->settingRepository));
        }
    }
}
