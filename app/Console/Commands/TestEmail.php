<?php

namespace App\Console\Commands;

use App\Notifications\ConfirmEmail;
use App\Notifications\SubscriptionEndsEmail;
use App\User;
use Illuminate\Console\Command;

class TestEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::where('email','yarik473@gmail.com')->first();

        $user->notify(new SubscriptionEndsEmail());

    }
}
