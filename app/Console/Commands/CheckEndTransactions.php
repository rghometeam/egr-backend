<?php

namespace App\Console\Commands;

use App\Models\Transaction;
use App\Repositories\SettingRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\UserRepository;
use Illuminate\Console\Command;

/**
 * Class CheckEndTransactions
 * @package App\Console\Commands
 */
class CheckEndTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:end';

    /**
     * @var TransactionRepository
     */
    protected $transactionRepository;

    /**
     * @var SettingRepository
     */
    protected $settingRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * CheckEndTransactions constructor.
     *
     * @param TransactionRepository $transactionRepository
     * @param SettingRepository $settingRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        SettingRepository $settingRepository,
        UserRepository $userRepository
    )
    {
        $this->settingRepository = $settingRepository;
        $this->transactionRepository = $transactionRepository;
        $this->userRepository = $userRepository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pendingTransactions = $this->userRepository->end();

        /** @var Transaction $transaction */
        foreach ($pendingTransactions as $transaction) {
            //TODO if user = admin send to logs group and dont makeUserSimple
            $this->userRepository->makeUserSimple($transaction);
        }

//        /** @var Transaction $transaction */
//        foreach ($pendingTransactions as $transaction) {
//            Mail::to($transaction->user)->send(new SubscriptionEnds($this->settingRepository));
//        }
    }
}
