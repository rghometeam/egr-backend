<?php

namespace App\Console\Commands;

use App\Repositories\ReferralTrackRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Console\Command;

class FillBrokenBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:fill-broken-balance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var ReferralTrackRepository
     */
    protected $referralTrackRepository;

    /**
     * FillBrokenBalance constructor.
     * @param ReferralTrackRepository $referralTrackRepository
     */
    public function __construct(ReferralTrackRepository $referralTrackRepository)
    {
        parent::__construct();

        $this->referralTrackRepository = $referralTrackRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user) {
            $payedUsers = $this->referralTrackRepository->getPayedUsers($user);

            if (!count($payedUsers)) continue;

            foreach ($payedUsers as $payedUser) {
                foreach ($payedUser->transactions as $transaction) {
                    $balance = ($user->balance + ($transaction->price / 100) * 10);
                    $user->balance = $balance;
                    $user->save();
                }
            }
        }
    }
}
