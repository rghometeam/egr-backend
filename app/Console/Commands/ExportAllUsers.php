<?php

namespace App\Console\Commands;

use App\Exports\AllUsers;
use App\Exports\GroupMembers;
use App\Models\ReferralTrack;
use App\Models\Transaction;
use App\Models\Withdrawal;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Console\Command;

class ExportAllUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $users = $this->userRepository->allWithoutPagination()->toArray();
        $i = 0;
        foreach ($users as &$user) {
//            $i += 1;
//            $user['num'] = $i;
            $user['role'] = $user['roles'][0]['name'];
            unset($user['roles']);
        }

//        dump($users);

//        dump($this->userRepository->all()->toArray());

//        (new AllUsers($users))->store('./dump_users.xlsx', 'local');

        foreach ($users as $user) {
            if ($user['role'] !== 'ROLE_USER') continue;

            $transactions = Transaction::where('user_id', $user['id'])->get();
            foreach ($transactions as $transaction) {
                $transaction->delete();
            }

            $referralTracks = ReferralTrack::where('referred_by', $user['id'])->get();
            foreach ($referralTracks as $referralTrack) {
                $referralTrack->delete();
            }

            $withdrawalsFrom = Withdrawal::where('from_user_id', $user['id'])->get();
            foreach ($withdrawalsFrom as $withdrawalFrom) {
                $withdrawalFrom->delete();
            }

            $withdrawalsTo = Withdrawal::where('to_user_id', $user['id'])->get();
            foreach ($withdrawalsTo as $withdrawalTo) {
                $withdrawalTo->delete();
            }

            User::where('id', $user['id'])->delete();
        }
    }
}
