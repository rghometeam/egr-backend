<?php

namespace App\Console\Commands;

use App\Repositories\UserRepository;
use App\User;
use Illuminate\Console\Command;

class TestUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::where('id', 213)->first();

//        (new UserRepository($user))->makeUserSimple($user);
        (new UserRepository($user))->makeUserPremium($user);
    }
}
