<?php

namespace App\Console\Commands;

use App\Repositories\UserRepository;
use App\User;
use Illuminate\Console\Command;

class SetAffiliateId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:affiliate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereNull('affiliate_id')->get();
        foreach ($users as $user) {
            $user->affiliate_id = (new UserRepository(new User()))->getRandomCode('affiliate_id');
            $user->save();
        }
    }
}
