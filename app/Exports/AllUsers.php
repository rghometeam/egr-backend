<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 11.04.2018
 * Time: 13:32
 */

namespace App\Exports;


use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * Class GroupMembers
 * @package App\Exports
 */
class AllUsers implements FromCollection
{
    use Exportable;

    /**
     * @var mixed
     */
    private $data;

    /**
     * @var int|null
     */
    private $option;

    /**
     * GroupMembers constructor.
     * @param mixed $data
     * @param int|null $option
     */
    public function __construct($data = [], int $option = null)
    {
        $this->data = $data;
        $this->option = $option;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return new Collection($this->data);
    }
}