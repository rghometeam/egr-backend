<?php

namespace App\Repositories;

use App\Models\Owner;
use App\Models\Setting;
use App\Role;
use App\User;

/**
 * Class SettingRepository
 * @package App\Repositories
 */
class SettingRepository
{
    /**
     * @var Setting
     */
    protected $setting;

    /**
     * SettingRepository constructor.
     * @param Setting $setting
     */
    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
    }

    /**
     * @return array
     */
    public function all()
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            return $this->setting->where('owner_id', $this->getOwnerId())->get();

        }
        elseif (Role::ROLE_SUPER_ADMIN === $user['roles'][0]['id'])  {
            return $this->setting->get();
        }
        else {
            return $this->setting->where('owner_id', $user['owner_id'])->get();
        }

    }

    /**
     * @return array
     */
    public function getSettingsForAdminPay() {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        return $this->setting->where('owner_id', $user['owner_id'])->get();
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        $userId = auth()->user()->id;

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('user_id', $userId)->first();

        return $owner['id'];
    }

    /**
     * @param int $id
     * @param array $data
     * @return Setting
     */
    public function update(int $id, array $data)
    {
        /** @var Setting $setting */
        $setting = $this->get($id);

        $setting->fill($data)->save();

        return $setting;

    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $setting = $this->setting
            ->where('id', $id)
            ->first()
        ;

        return $setting;
    }

}