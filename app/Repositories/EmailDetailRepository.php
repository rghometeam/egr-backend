<?php

namespace App\Repositories;

use App\Models\EmailDetail;
use App\Models\Owner;
use App\Role;
use App\User;

/**
 * Class EmailDetailRepository
 * @package App\Repositories
 */
class EmailDetailRepository
{
    /**
     * @var EmailDetail
     */
    protected $emailDetail;


    /**
     * EmailDetailRepository constructor.
     * @param EmailDetail $emailDetail
     */
    public function __construct(EmailDetail $emailDetail)
    {
        $this->emailDetail = $emailDetail;
    }

    /**
     * @return array
     */
    public function all()
    {

        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            return $this->emailDetail->where('owner_id', $this->getOwnerId())->get();
        }
        else  {
            return $this->emailDetail->get();
        }

    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        $userId = auth()->user()->id;

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('user_id', $userId)->first();

        return $owner['id'];
    }

    /**
     * @param int $id
     * @param array $data
     * @return EmailDetail
     */
    public function update(int $id, array $data)
    {
        /** @var EmailDetail $emailDetail */
        $emailDetail = $this->get($id);

        $emailDetail->fill($data)->save();

        return $emailDetail;

    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $emailDetail = $this->emailDetail
            ->where('id', $id)
            ->first()
        ;

        return $emailDetail;
    }

}