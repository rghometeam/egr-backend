<?php

namespace App\Repositories;

use App\Models\Owner;
use App\Models\Setting;
use App\Models\Transaction;
use App\Role;
use App\Services\CryptoCurrency;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

/**
 * Class TransactionRepository
 * @package App\Repositories
 */
class TransactionRepository extends Repository
{
    /**
     * @var Transaction
     */
    protected $transaction;


    /**
     * TransactionRepository constructor.
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @param int $userId
     * @param int|null $except
     * @return mixed
     */
    public function getLatestByUser(int $userId, int $except = null)
    {
        $transaction = $this->transaction
            ->where('status', Transaction::STATUS_SUCCESS)
            ->where('user_id', $userId)
            ->where('finished_date', '>', Carbon::now()->toDateTimeString())
            ->orderBy('finished_date', 'desc')
        ;

        if ($except) {
            $transaction->where('id', '!=', $except);
        }

        return $transaction->first();
    }

    /**
     * @return mixed
     */
    public function pending()
    {
        return $this->transaction->where('status', Transaction::STATUS_PENDING)->get();
    }

    /**
     * @return mixed
     */
    public function failed()
    {
        return $this->transaction->where('status', Transaction::STATUS_FAILED)->get();
    }

    /**
     * @return mixed
     */
    public function ending()
    {
        return $this->transaction
            ->where('status', Transaction::STATUS_SUCCESS)
            ->where('finished_date', '<', Carbon::now()->addWeek()->toDateTimeString())
            ->where('notified_ending', 0)
            ->get()
        ;
    }

    /**
     * @return mixed
     */
    public function toBlackList()
    {
        return $this->transaction
            ->where('status', '!=', Transaction::STATUS_PENDING)
            ->where('updated_at', '<', Carbon::now()->subMinutes(20)->toDateTimeString())
            ->get()
        ;
    }

    /**
     * @return mixed
     */
    public function end()
    {
        return $this->transaction
            ->where('status', Transaction::STATUS_SUCCESS)
            ->where('finished_date', '<', Carbon::now()->toDateTimeString())
            ->get()
        ;
    }


    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        $userId = auth()->user()->id;

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('user_id', $userId)->first();

        return $owner['id'];
    }

//    /**
//     * @return Transaction
//     */
//    public function allForAdmin()
//    {
//        /**
//         * var User $user
//         */
//        $user = User::where('id', auth()->user()->id)->with('roles')->first();
//
//        /** @var Transaction $transactions */
//        $transactions['all'] = $this->transaction->where('owner_id', $this->getOwnerId())->paginate($this->limit);
//
//        $transactions['my'] = $this->transaction->where('user_id', $user['id'])->paginate($this->limit);
//
//        return $transactions;
//    }

    /**
     * @return Transaction
     */
    public function all()
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            /** @var Transaction $transactions */
            $transactions = $this->transaction->where('owner_id', $this->getOwnerId())->paginate($this->limit);
        } elseif (Role::ROLE_SUPER_ADMIN === $user['roles'][0]['id']) {
            /** @var Transaction $transactions */
            $transactions = $this->transaction->paginate($this->limit);
        }
        else {
            /** @var Transaction $transactions */
            $transactions = $this->transaction->where('user_id', $user['id'])->paginate($this->limit);
        }

        return $transactions;
    }


    /**
     * @param $data
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function filter($data)
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            // todo separate transactions for admin
            /** @var Transaction $transactions */
//            $transactions = $this->transaction->where('user_id', $user['id']);
            /** @var Transaction $transactions */
            $transactions = $this->transaction->where('owner_id', $this->getOwnerId());
        } elseif (Role::ROLE_SUPER_ADMIN === $user['roles'][0]['id']) {
            /** @var Transaction $transactions */
            $transactions = $this->transaction;
        }
        else {
            /** @var Transaction $transactions */
            $transactions = $this->transaction->where('user_id', $user['id']);
        }



        if ($data['search']) {
            $transactions = $transactions
                ->where('tx_id', 'like', '%' . $data['search'] . '%')
//                ->orWhereHas('user', function ($query) use ($data) {
//                    $query
//                        ->orWhere('telegram_name','like', '%' . $data['search'] . '%')
//                        ->orWhere('phone','like', '%' . $data['search'] . '%')
//                        ->orWhere('first_name','like', '%' . $data['search'] . '%')
//                        ->orWhere('last_name','like', '%' . $data['search'] . '%')
//                    ;
//                })

            ;
        }

        if ($data['status_id']) {
            if ((int) $data['status_id'] === 99) {
                $data['status_id'] = 0;
            }

            $transactions = $transactions->where('status', $data['status_id']);
        }

        return $transactions->paginate($this->limit);
    }

    /**
     * @param array $data
     * @return Transaction
     */
    public function create(array $data)
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->first();

        $data['owner_id'] = $user['owner_id'];

        $this->transaction->fill($data)->save();

        return $this->transaction;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Transaction
     */
    public function update(int $id, array $data)
    {
        /** @var Transaction $transaction */
        $transaction = $this->get($id);

        $transaction->fill($data)->save();

        return $transaction;

    }


    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $transaction = $this->transaction
            ->where('id', $id)
            ->first()
        ;

        return $transaction;
    }

    /**
     * @param $tx_id
     * @param $user_id
     * @return mixed
     */
    public function getUnique($tx_id, $user_id)
    {
        return $this->transaction
            ->where('tx_id', $tx_id)
            ->where('user_id', $user_id)
            ->first()
        ;
    }

    /**
     * @param int $id
     * @return array
     */
    public function processTransaction(int $id)
    {
        $checker = new CryptoCurrency();

        $transaction = $this->get($id);

        $statusResult = $checker->getStatus($transaction);

//        $statusResult['status'] = Transaction::STATUS_SUCCESS;
//        $statusResult['message'] = true;
//        $statusResult['is_correct_fee'] = true;

        if ($statusResult['status'] === Transaction::STATUS_SUCCESS) {
            if ((int) $transaction->status === Transaction::STATUS_PENDING) {
                $finishedDate = Carbon::now()->addDays((int) $transaction->package->days);

                if ($latestTransaction = $this->getLatestByUser($transaction->user_id, $id)) {
                    $finishedDate = Carbon::parse($latestTransaction->finished_date)->addDays((int) $transaction->package->days);
                }

                if ($transaction->user->finished_date) {
                    $finishedDate = Carbon::parse($transaction->user->finished_date)->addDays((int) $transaction->package->days);
                }

                $transaction->is_correct_fee = $statusResult['is_correct_fee'] ?? false;
                $transaction->status = Transaction::STATUS_SUCCESS;
//                $transaction->status = Transaction::STATUS_PENDING;
                $transaction->finished_date = $finishedDate;
                $transaction->save();

                $transaction->user->finished_date = $finishedDate;
                $transaction->user->need_discount = false;
                $transaction->user->save();

                if ($transaction->user->referred_by) {
                    try {
                        $referredUser = User::whereId($transaction->user->referred_by)->first();
                        $balance = ($referredUser->balance + ($transaction->price / 100) * 10);
                        $referredUser->balance = $balance;
                        $referredUser->save();
                    } catch (\Exception $exception) { }
                }

                /**
                 * var User $user
                 */
                $user = User::where('id', auth()->user()->id)->with('roles')->first();

                if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
                    (new UserRepository($transaction->user))->sendMessageToLogsChannel($transaction->user);
                } else {
                    (new UserRepository($transaction->user))->makeUserPremium($transaction->user);
                }

                LogRepository::setInfo('payment', $statusResult['message'], $transaction->id);
            }
        } else if ($statusResult['status'] === Transaction::STATUS_FAILED) {
            $transaction->status = Transaction::STATUS_FAILED;
            $transaction->save();

            LogRepository::setError('payment', $statusResult['message'], $transaction->id);
        }

        $transactionResult = $statusResult;
        $transactionResult['transaction'] = $transaction;

        return compact('transactionResult');
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(int $id)
    {
        /** @var Transaction $transaction */
        $transaction = $this->get($id);

        $transaction->delete();

        return new JsonResponse(true);
    }

}