<?php

namespace App\Repositories;

use App\Models\Owner;
use App\Models\Package;
use App\Role;
use App\User;
use Illuminate\Http\JsonResponse;


/**
 * Class PackageRepository
 * @package App\Repositories
 */
class PackageRepository
{
    /**
     * @var Package
     */
    protected $package;


    /**
     * PackageRepository constructor.
     * @param Package $package
     */
    public function __construct(Package $package)
    {
        $this->package = $package;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            $packages = $this->package->where('owner_id', $this->getOwnerId())->get();
        } elseif (Role::ROLE_SUPER_ADMIN === $user['roles'][0]['id']) {
            $packages = $this->package->get();
        }
        else  {
            $packages = $this->package->where('owner_id', $user['owner_id'])->get();
        }

        return $packages;
    }

    public function getPackagesForAdmin()
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            $packages['all'] = $this->package->where('owner_id', $user['owner_id'])->get();
            $packages['for_admin_users'] = $this->package->where('owner_id', $this->getOwnerId())->get();
        } else {
            $packages['all'] = $this->package->where('owner_id', $this->getOwnerId())->get();
        }

        return $packages;
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        $userId = auth()->user()->id;

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('user_id', $userId)->first();

        return $owner['id'];
    }


    /**
     * @param array $data
     * @return Package
     */
    public function create(array $data)
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            $data['owner_id'] = $this->getOwnerId();
        }

        $this->package->fill($data)->save();

        return $this->package;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Package
     */
    public function update(int $id, array $data)
    {
        /** @var Package $package */
        $package = $this->get($id);

        $package->fill($data)->save();

        return $package;

    }


    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $package = $this->package
            ->where('id', $id)
            ->first()
        ;

        return $package;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete(int $id)
    {
        /** @var Package $package */
        $package = $this->get($id);

        $package->delete();

        return new JsonResponse(true);
    }

}