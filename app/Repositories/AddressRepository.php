<?php

namespace App\Repositories;

use App\Models\Address;
use App\Models\Owner;
use App\Role;
use App\User;
use Illuminate\Http\JsonResponse;


class AddressRepository
{
    /**
     * @var Address
     */
    protected $address;



    public function __construct(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            $addresses = $this->address->where('owner_id', $this->getOwnerId())->get();
        }
        else  {
            $addresses = $this->address->get();
        }

        return $addresses;
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        $userId = auth()->user()->id;

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('user_id', $userId)->first();

        return $owner['id'];
    }

    /**
     * @param $type
     * @param $ownerId
     * @return mixed
     */
    public function getAllByType($type, $ownerId)
    {
        $addresses = $this->address->where('owner_id', $ownerId)->whereType($type)->get();

        return $addresses;
    }

    /**
     * @param array $data
     * @return Address
     */
    public function create(array $data)
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            $data['owner_id'] = $this->getOwnerId();
        }

        $this->address->fill($data)->save();

        return $this->address;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Address
     */
    public function update(int $id, array $data)
    {
        /** @var Address $address */
        $address = $this->get($id);

        $address->fill($data)->save();

        return $address;

    }


    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $address = $this->address
            ->where('id', $id)
            ->first()
        ;

        return $address;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete(int $id)
    {
        /** @var Address $address */
        $address = $this->get($id);

        $address->delete();

        return new JsonResponse(true);
    }

}