<?php

namespace App\Repositories;

use App\Models\Signal;
use App\Models\Target;

/**
 * Class SignalRepository
 * @package App\Repositories
 */
class TargetRepository
{
    /**
     * @var Target
     */
    protected $target;


    /**
     * TargetRepository constructor.
     * @param Target $target
     */
    public function __construct(Target $target)
    {
        $this->target = $target;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $this->target->insert($data);

        return $this->target;
    }

//    /**
//     * @return array
//     */
//    public function all()
//    {
//        return $this->signal->get();
//    }
//
//
    /**
     * @param int $id
     * @param array $data
     * @return Signal
     */
    public function update(int $id, array $data)
    {
        /** @var Signal $signal */
        $signal = $this->get($id);

        $signal->fill($data)->save();

        return $signal;

    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $signal = $this->target
            ->where('id', $id)
            ->first()
        ;

        return $signal;
    }
//
//    /**
//     * @param $id
//     * @return mixed
//     */
//    public function delete(int $id)
//    {
//        /** @var Signal $signal */
//        $signal = $this->get($id);
//
//        $signal->delete();
//
//        return new JsonResponse(true);
//    }

}