<?php

namespace App\Repositories;

use App\Models\Log;
use App\Models\Owner;
use App\Role;
use App\User;
use Illuminate\Http\JsonResponse;


/**
 * Class PackageRepository
 * @package App\Repositories
 */
class LogRepository
{
    const STATUS_INFO = 1;
    const STATUS_WARNING = 2;
    const STATUS_ERROR = 3;

    /**
     * @var Log
     */
    protected $log;

    //TODO when create log set owner_id
    /**
     * LogRepository constructor.
     * @param Log $log
     */
    public function __construct(Log $log)
    {
        $this->log = $log;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            $logs = $this->log->where('owner_id', $this->getOwnerId())->get();
        }
        else  {
            $logs = $this->log->get();
        }


        return $logs;
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        $userId = auth()->user()->id;

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('user_id', $userId)->first();

        return $owner['id'];
    }

    /**
     * @param array $data
     * @return Log
     */
    public function create(array $data)
    {
        $data['owner_id'] = $this->getOwnerId();

        $this->log->fill($data)->save();

        return $this->log;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Log
     */
    public function update(int $id, array $data)
    {
        /** @var Log $log */
        $log = $this->get($id);

        $log->fill($data)->save();

        return $log;

    }


    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $log = $this->log
            ->where('id', $id)
            ->first()
        ;

        return $log;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete(int $id)
    {
        /** @var Log $log */
        $log = $this->get($id);

        $log->delete();

        return new JsonResponse(true);
    }

    /**
     * @param $provider
     * @param $data
     * @param int|null $entity
     * @return bool
     */
    public static function setError($provider, $data, int $entity = null)
    {
        return self::setLog($provider, self::STATUS_ERROR, $data, $entity);
    }

    /**
     * @param $provider
     * @param $data
     * @param int|null $entity
     * @return bool
     */
    public static function setInfo($provider, $data, int $entity = null)
    {
        return self::setLog($provider, self::STATUS_INFO, $data, $entity);
    }

    /**
     * @param $provider
     * @param $data
     * @param int|null $entity
     * @return bool
     */
    public static function setWarning($provider, $data, int $entity = null)
    {
        return self::setLog($provider, self::STATUS_WARNING, $data, $entity);
    }

    /**
     * @param $provider
     * @param $status
     * @param $data
     * @param int|null $entity
     * @return bool
     */
    private static function setLog($provider, $status, $data, int $entity = null)
    {
        $log = new Log();
        $log->provider = $provider;
        $log->status = $status;
        $log->data = $data;
        $log->entity_id = $entity;
        $log->save();

        return true;
    }

}