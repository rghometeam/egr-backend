<?php
/**
 * Created by PhpStorm.
 * User: rghome
 * Date: 4/28/18
 * Time: 01:13
 */

namespace App\Repositories;


use App\Models\Owner;
use App\Models\Withdrawal;
use App\Role;
use App\Services\PyrogramService;
use App\User;
use Illuminate\Validation\ValidationException;

/**
 * Class WithdrawalRepository
 * @package App\Repositories
 */
class WithdrawalRepository
{
    /**
     * @var Withdrawal
     */
    protected $withdrawal;

    /**
     * @var float
     */
    protected $min = 0.01;

    /**
     * WithdrawalRepository constructor.
     * @param Withdrawal $withdrawal
     */
    public function __construct(Withdrawal $withdrawal)
    {
        $this->withdrawal = $withdrawal;
    }

    /**
     * @param array $data
     * @param $user
     * @return Withdrawal
     */
    public function create(array $data, $user)
    {
        /**
         * @var Owner $owner
         */
        $owner =  Owner::where('user_id', $user->id)->first();

        if (!isset($data['amount'])) {
            throw ValidationException::withMessages(['fill_amount']);
        }

        if ((double) $data['amount'] < $this->min) {
            throw ValidationException::withMessages(['min']);
        }


        if ((double) $data['amount'] > (double) $user->balance) {
            throw ValidationException::withMessages(['insufficient_balance']);
        }

        try {
            $toUser = User::where('telegram_name', $data['to_user_id'])->firstOrFail();
        } catch (\Exception $exception) {
            throw ValidationException::withMessages(['to_user_not_found']);
        }

        $user->balance = ((double) $user->balance - (double) $data['amount']);
        $user->save();

        $toUser->balance = ((double) $toUser->balance + (double) $data['amount']);
        $toUser->save();

        $this->withdrawal->owner_id = $owner['id'];

        $this->withdrawal->to_user_id = $toUser->id;
        $this->withdrawal->from_user_id = $user->id;
        $this->withdrawal->amount = (double) $data['amount'];
        $this->withdrawal->save();

        $withdrawal = $this->get((int) $this->withdrawal->id);

        try {
            $message = [
                'Withdrawal:',
                'From: @' . $withdrawal->fromUser->telegram_name,
                'To User: @' . $withdrawal->toUser->telegram_name,
                'Amount: ' . $withdrawal->amount,
                'Created At: ' . $withdrawal->created_at
            ];
            $pyrogramService = new PyrogramService();
            $pyrogramService->publishToLogsChannel($message);
        } catch (\Exception $exception) {
            LogRepository::setError('telegram', $exception->getMessage(), $user->id);
        }

        return $withdrawal;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function get(int $id)
    {
        return $this->withdrawal->where('id', $id)->first();
    }

    /**
     * @return mixed
     */
    public function all()
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            /** @var Withdrawal $withdrawal */
            $withdrawal = $this->withdrawal->where('owner_id', $this->getOwnerId())->get();
        }
        elseif (Role::ROLE_SUPER_ADMIN === $user['roles'][0]['id']) {
            /** @var Withdrawal $withdrawal */
            $withdrawal = $this->withdrawal->get();
        }
        else {
            /** @var Withdrawal $withdrawal */
            $withdrawal = $this->withdrawal->where('from_user_id', $user['id'])->get();
        }

        return $withdrawal;
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        $userId = auth()->user()->id;

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('user_id', $userId)->first();

        return $owner['id'];
    }

}