<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 21.04.2018
 * Time: 13:33
 */

namespace App\Repositories;


use App\Models\ReferralTrack;
use App\Models\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * Class ReferralTrackRepository
 * @package App\Repositories
 */
class ReferralTrackRepository
{
    /**
     * @var ReferralTrack
     */
    private $referralTrack;

    /**
     * ReferralTrackRepository constructor.
     * @param ReferralTrack $referralTrack
     */
    public function __construct(ReferralTrack $referralTrack)
    {
        $this->referralTrack = $referralTrack;
    }

    /**
     * @param $ip
     * @param $code
     * @return ReferralTrack
     */
    public function track($ip, $code)
    {
        $this->referralTrack->ip = $ip;
        $this->referralTrack->referred_by = User::whereAffiliateId($code)->first()->id;
        $this->referralTrack->save();

        return $this->referralTrack;
    }

    public function statistic(Authenticatable $user)
    {
        $visits = $this->getVisitsStatistic($user);
        $registered = $this->getRegisteredStatistic($user);
        $payed = $this->getPayedUsersStatistic($user);

        return compact('visits', 'registered', 'payed');
    }

    /**
     * @param Authenticatable $user
     * @return array
     */
    private function getVisitsStatistic(Authenticatable $user)
    {
        $tracks = $this->referralTrack->where('referred_by', $user->id)->get();

        $visits = [
            'total' => count($tracks),
            'groups' => []
        ];

        foreach ($tracks as $track) {
            $date = Carbon::parse($track->created_at)->format('Y-m-d');

            if (!isset($visits['groups'][$date])) {
                $visits['groups'][$date] = 0;
            }

            ++$visits['groups'][$date];
        }

        return $visits;
    }

    /**
     * @param Authenticatable $user
     * @return array
     */
    private function getRegisteredStatistic(Authenticatable $user)
    {
        $registeredUsers = User::whereReferredBy($user->id)->get();

        $registered = [
            'total' => count($registeredUsers),
            'groups' => []
        ];

        foreach ($registeredUsers as $user) {
            $date = Carbon::parse($user->created_at)->format('Y-m-d');

            if (!isset($registered['groups'][$date])) {
                $registered['groups'][$date] = 0;
            }

            ++$registered['groups'][$date];
        }

        return $registered;
    }

    /**
     * @param Authenticatable $user
     * @return mixed
     */
    public function getPayedUsers(Authenticatable $user)
    {
        return User::whereReferredBy($user->id)->whereHas('transactions', function ($query) {
            $query->whereStatus(Transaction::STATUS_SUCCESS);
        })->with('transactions')->get();
    }

    /**
     * @param Authenticatable $user
     * @return array
     */
    private function getPayedUsersStatistic(Authenticatable $user)
    {
        $registeredUsers = $this->getPayedUsers($user);

        $payed = [
            'total' => count($registeredUsers),
            'groups' => []
        ];

        foreach ($registeredUsers as $user) {
            $date = Carbon::parse($user->created_at)->format('Y-m-d');

            if (!isset($payed['groups'][$date])) {
                $payed['groups'][$date] = 0;
            }

            ++$payed['groups'][$date];
        }

        return $payed;
    }
}