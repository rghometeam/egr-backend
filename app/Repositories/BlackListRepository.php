<?php

namespace App\Repositories;

use App\Models\BlackList;
use App\Role;
use App\User;
use Illuminate\Http\JsonResponse;


/**
 * Class PackageRepository
 * @package App\Repositories
 */
class BlackListRepository
{
    /**
     * @var BlackList
     */
    protected $blackList;

    /**
     * BlackListRepository constructor.
     * @param BlackList $blackList
     */
    public function __construct(BlackList $blackList)
    {
        $this->blackList = $blackList;
    }

    /**
     * @param $ownerId
     * @return mixed
     */
    public function all($ownerId)
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            $blackLists = $this->blackList->where('owner_id',$ownerId)->get();
        }
        else  {
            $blackLists = $this->blackList->get();
        }

        return $blackLists;
    }


    /**
     * @param array $data
     * @return BlackList
     */
    public function create(array $data)
    {
        $this->blackList->fill($data)->save();

        return $this->blackList;
    }

    /**
     * @param int $id
     * @param array $data
     * @return BlackList
     */
    public function update(int $id, array $data)
    {
        /** @var BlackList $blackList */
        $blackList = $this->get($id);

        $blackList->fill($data)->save();

        return $blackList;

    }


    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $blackList = $this->blackList
            ->where('id', $id)
            ->first()
        ;

        return $blackList;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete(int $id)
    {
        /** @var BlackList $blackList */
        $blackList= $this->get($id);

        $blackList->delete();

        return new JsonResponse(true);
    }

}