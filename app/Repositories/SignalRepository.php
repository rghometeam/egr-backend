<?php

namespace App\Repositories;

use App\Models\Owner;
use App\Models\Signal;
use App\Role;
use App\User;
use Illuminate\Http\JsonResponse;

/**
 * Class SignalRepository
 * @package App\Repositories
 */
class SignalRepository
{
    /**
     * @var Signal
     */
    protected $signal;


    /**
     * SignalRepository constructor.
     * @param Signal $signal
     */
    public function __construct(Signal $signal)
    {
        $this->signal = $signal;
    }

    /**
     * @param $status
     * @return mixed
     */
    public function all($status)
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            return $this->signal
                ->where('owner_id', $this->getOwnerId())
                ->where('status', $status)
                ->with('targets')
                ->get()
                ;
        }
        else  {
            return $this->signal
                ->where('status', $status)
                ->with('targets')
                ->get()
                ;
        }


    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        $userId = auth()->user()->id;

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('user_id', $userId)->first();

        return $owner['id'];
    }

    /**
     * @param int $id
     * @param array $data
     * @return Signal
     */
    public function update(int $id, array $data)
    {
        /** @var Signal $signal */
        $signal = $this->get($id);

        $signal->fill($data)->save();

        return $signal;

    }

    /**
     * @param array $data
     * @return Signal
     */
    public function create(array $data)
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            $data['owner_id'] = $this->getOwnerId();
        }

        $this->signal->fill($data)->save();

        return $this->signal;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $signal = $this->signal
            ->where('id', $id)
            ->with('targets')
            ->first()
        ;

        return $signal;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete(int $id)
    {
        /** @var Signal $signal */
        $signal = $this->get($id);

        $signal->delete();

        return new JsonResponse(true);
    }

}