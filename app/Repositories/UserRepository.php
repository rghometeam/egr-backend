<?php

namespace App\Repositories;

use App\Models\Owner;
use App\Role;
use App\Services\MTProtoService;
use App\Services\PyrogramService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;


/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends Repository
{
    /**
     * @var User
     */
    protected $user;
    

    /**
     * UserRegistrationRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function all()
    {
        return $this->user
            ->with('roles')
//            ->orderBy('created_at', 'desc')
            ->paginate($this->limit)
        ;
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        $userId = auth()->user()->id;

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('user_id', $userId)->first();

        return $owner['id'];
    }

    /**
     * @return mixed
     */
    public function getAdmins()
    {
        return $this->user
            ->whereHas('roles', function($q){
                $q->where('id', Role::ROLE_ADMIN)
                ->orWhere('id', Role::ROLE_SUPER_ADMIN);
            })
            ->with('roles')
            ->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function allWithoutPagination()
    {
        return $this->user
            ->with('roles')
            ->get()
        ;
    }

    /**
     * @param $data
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function filter($data)
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            $users = $this->user->where('owner_id', $this->getOwnerId())->with('roles');
        } else {
            $users = $this->user->with('roles');
        }

        if ($data['search']) {
            $users
                ->where('email','like', '%' . $data['search'] . '%')
                ->orWhere('telegram_name','like', '%' . $data['search'] . '%')
                ->orWhere('phone','like', '%' . $data['search'] . '%')
                ->orWhere('first_name','like', '%' . $data['search'] . '%')
                ->orWhere('last_name','like', '%' . $data['search'] . '%')
            ;
        }

        if (isset($data['role_id']) && $data['role_id']) {
            $users
                ->whereHas('roles', function ($query) use ($data) {
                    $query->where('id', $data['role_id']);
                });
        }

        if (isset($data['sortBy'])) {
            $users->orderBy($data['sortBy'], (($data['sortDesc'] === 'true') ? 'DESC' : 'ASC'));
        }

        return $users->paginate($this->limit);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function exportForCryptogram()
    {
        return User::select('email')->whereNotNull('email')->whereHas('roles', function ($query) {
            $query->whereIn('id', [
                Role::ROLE_ADMIN, Role::ROLE_FREE, Role::ROLE_PREMIUM, Role::ROLE_PREMIUM
            ]);
        })->get();
    }

    /**
     * @param User|null $user
     * @return User
     */
    public function sendMessageToLogsChannel(User $user = null)
    {
        $mtProtoService = new PyrogramService();

        if (is_null($user)) {
            $user = $this->user;
        }

        try {
            $message = 'Admin @' . $user->telegram_name . '  paid subscribe. Need set domain';
            $mtProtoService->publishToLogsChannel($message);
        } catch (\Exception $exception) {
            LogRepository::setError('telegram', $exception->getMessage(), $user->id);
        }

        return $user;
    }

    /**
     * @param User|null $user
     * @return User
     */
    public function makeUserPremium(User $user = null)
    {
        $mtProtoService = new PyrogramService();
        $botMtProto = new MTProtoService();

        if (is_null($user)) {
            $user = $this->user;
        }

        $user->detachRoles();
        $user->attachRole(Role::ROLE_PREMIUM);
        $user->save();

        $role = Role::find(Role::ROLE_PREMIUM);

        $user->roles = [$role];

        try {
            $mtProtoService->inviteToChannel($user);
        } catch (\Exception $exception) {
            LogRepository::setError('telegram', $exception->getMessage(), $user->id);
        }


        try {
            $message = 'Member @' . $user->telegram_name . ' Need to be Added. New Role:  ' . $user->roles[0]->display_name;
            $mtProtoService->publishToLogsChannel($message);
//            $botMtProto->inviteToChannel($user);
        } catch (\Exception $exception) {
            LogRepository::setError('telegram', $exception->getMessage(), $user->id);
        }

        return $user;
    }

    /**
     * @param User $user
     * @return int
     */
    public function setUserAuthCode(User $user)
    {
        $randomInt = $this->getRandomCode();

        $user->auth_code = $randomInt;
        $user->save();

        return $randomInt;
    }

    /**
     * @param User $user
     * @param array $telegramData
     */
    public function updateUserByTelegramInfo(User $user, array $telegramData)
    {
//        if (!$user->first_name && isset($telegramData['first_name'])) {
//            $user->first_name = $telegramData['first_name'];
//        }
//
//        if (!$user->last_name && isset($telegramData['last_name'])) {
//            $user->last_name = $telegramData['last_name'];
//        }

        if (!$user->telegram_name && isset($telegramData['username'])) {
            $user->telegram_name = $telegramData['username'];
        }

        if (isset($telegramData['id'])) {
            $user->telegram_id = $telegramData['id'];
        }

        $user->save();
    }

    /**
     * @param string $key
     * @return int
     */
    public function getRandomCode($key = 'auth_code')
    {
        $randomInt = rand(10000, 99999);

        try {
            User::where($key, $randomInt)->firstOrFail();

            return $this->getRandomCode();
        } catch (\Exception $exception) {
            return $randomInt;
        }
    }

    /**
     * @param User|null $user
     * @return User
     */
    public function makeUserSimple(User $user = null)
    {
        $mtProtoService = new PyrogramService();
        $botMtProto = new MTProtoService();

        if (is_null($user)) {
            $user = $this->user;
        }

        $user->detachRoles();
        $user->attachRole(Role::ROLE_USER);
        $user->finished_date = null;
        $user->save();

        $role = Role::find(Role::ROLE_USER);

        $user->roles = [$role];

        try {
            $mtProtoService->banInChannel($user);
        } catch (\Exception $exception) {
            LogRepository::setError('telegram', $exception->getMessage(), $user->id);
        }

        try {
            $message = 'Member @' . $user->telegram_name . ' Need to be kicked. New Role:  ' . $user->roles[0]->display_name;
            $mtProtoService->publishToLogsChannel($message);
//            $botMtProto->banInChannel($user);
        } catch (\Exception $exception) {
            LogRepository::setError('telegram', $exception->getMessage(), $user->id);
        }

        return $user;
    }


    /**
     * @param array $data
     * @return User
     */
    public function create(array $data)
    {
//        if (isset($data['password'])) {
//            $data['password'] = bcrypt($data['password']);
//        }

        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            $data['owner_id'] = $this->getOwnerId();
        }

        $data['telegram_name'] = trim($data['telegram_name'],'@');
        $data['confirmed'] = true;
        $data['affiliate_id'] = $this->getRandomCode('affiliate_id');

        $this->user->fill($data)->save();

        if (isset($data['roles'])) {
            $this->user->attachRole($data['roles']);
        } else {
            $this->user->attachRole(Role::ROLE_USER);
        }

        $user = User::where('id', $this->user->id)->with('roles')->first();

        return $user;
    }

    /**
     * @return mixed
     */
    public function end()
    {
        return $this->user
            ->where('finished_date', '<', Carbon::now()->toDateTimeString())
            ->get()
        ;
    }

    /**
     * @return mixed
     */
    public function ending()
    {
        return $this->user
            ->where('finished_date', '<', Carbon::now()->addWeek()->toDateTimeString())
            ->where('notified_ending', 0)
            ->get()
        ;
    }

    /**
     * @param int $id
     * @param array $data
     * @return User
     */
    public function update(int $id, array $data)
    {
        /** @var User $user */
        $user = $this->get($id);

        if (isset($data['owner_data']) && $data['roles'][0]['id'] == Role::ROLE_ADMIN) {
            Owner::where('user_id', $id)->update($data['owner_data']);
        }

        $data['telegram_name'] = trim($data['telegram_name'],'@');

        $user->fill($data)->save();

        if (isset($data['role'])) {
            if ($data['roles'][0]['id'] != $data['role']['id']) {
                $user->detachRole($data['roles'][0]['id']);
                $user->save();

                $user->attachRole($data['role']['id']);
                $user->save();

                $role = Role::find($data['role']['id']);

                $user->roles = [$role];

                $mtProtoService = new PyrogramService();

                if ($data['role']['id'] === Role::ROLE_FREE || $data['role']['id'] === Role::ROLE_PREMIUM || $data['role']['id'] === Role::ROLE_EDITOR) {
                    try {
                        $mtProtoService->inviteToChannel($user);
                    } catch (\Exception $exception) {
                        LogRepository::setError('telegram', $exception->getMessage(), $user->id);
                    }

                    try {
                        $message = 'Member @' . $user->telegram_name . ' Need to be Added. New Role:  ' . $user->roles[0]->display_name;
                        $mtProtoService->publishToLogsChannel($message);
//                        $botMtProto->inviteToChannel($user);
                    } catch (\Exception $exception) {
                        LogRepository::setError('telegram', $exception->getMessage(), $user->id);
                    }
                } elseif ($data['role']['id'] === Role::ROLE_USER) {
                    try {
                        $mtProtoService->banInChannel($user);
                    } catch (\Exception $exception) {
                        LogRepository::setError('telegram', $exception->getMessage(), $user->id);
                    }

                    try {
                        $message = 'Member @' . $user->telegram_name . ' Need to be kicked. New Role:  ' . $user->roles[0]->display_name;
                        $mtProtoService->publishToLogsChannel($message);
//                        $botMtProto->banInChannel($user);
                    } catch (\Exception $exception) {
                        LogRepository::setError('telegram', $exception->getMessage(), $user->id);
                    }
                }
            }
        }

        return $this->get($id);
    }

    /**
     * @param $id
     * @return User
     */
    public function get($id)
    {
        return $this->user
            ->where('id', $id)
            ->with('roles')
            ->with('owner')
            ->first()
        ;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        try
        {
            $this->get($id)->delete();
            return new JsonResponse(true);
        } catch (\Exception $exception)
        {
            return new JsonResponse(false);
        }

    }

}