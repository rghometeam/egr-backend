<?php

namespace App\Repositories;

use App\Models\Faq;
use App\Models\Owner;
use App\Role;
use App\User;
use Illuminate\Http\JsonResponse;

/**
 * Class FaqRepository
 * @package App\Repositories
 */
class FaqRepository
{
    /**
     * @var Faq
     */
    protected $faq;


    /**
     * FaqRepository constructor.
     * @param Faq $faq
     */
    public function __construct(Faq $faq)
    {
        $this->faq = $faq;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        /**
         * var User $user
         */
        $user = User::where('id', auth()->user()->id)->with('roles')->first();

        if (Role::ROLE_ADMIN === $user['roles'][0]['id']) {
            $faqs = $this->faq->where('owner_id', $this->getOwnerId())->get();

        }
        else  {
            $faqs = $this->faq->get();
        }

        return $faqs;
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        $userId = auth()->user()->id;

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('user_id', $userId)->first();

        return $owner['id'];
    }

    /**
     * @param array $data
     * @return Faq
     */
    public function create(array $data)
    {
        $data['owner_id'] = $this->getOwnerId();

        $this->faq->fill($data)->save();

        return $this->faq;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Faq
     */
    public function update(int $id, array $data)
    {
        /** @var Faq $faq */
        $faq = $this->get($id);

        $faq->fill($data)->save();

        return $faq;

    }


    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $faq = $this->faq
            ->where('id', $id)
            ->first()
        ;

        return $faq;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete(int $id)
    {
        /** @var Faq $faq */
        $faq = $this->get($id);

        $faq->delete();

        return new JsonResponse(true);
    }

}