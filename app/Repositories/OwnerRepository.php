<?php

namespace App\Repositories;

use App\Models\Owner;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;


/**
 * Class OwnerRepository
 * @package App\Repositories
 */
class OwnerRepository
{
    /**
     * @var Owner
     */
    protected $owner;


    /**
     * OwnerRepository constructor.
     * @param Owner $owner
     */
    public function __construct(Owner $owner)
    {
        $this->owner = $owner;
    }
    
    public function all()
    {
        $owner = $this->owner->with('users', 'profile')->get();

        return $owner;
    }


    /**
     * @param array $data
     * @return Owner
     */
    public function create(array $data)
    {
        $this->owner->fill($data)->save();

        $this->createSettingsAndEmailDetails($this->owner->id);

        return $this->owner;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Owner
     */
    public function update(int $id, array $data)
    {
        /** @var Owner $owner */
        $owner = $this->owner
            ->where('id', $id)
            ->first();

        $owner->fill($data)->save();

        return $owner;

    }


    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $owner = $this->owner
            ->where('id', $id)
            ->with('users')
            ->first()
        ;

        return $owner;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete(int $id)
    {
        /** @var Owner $owner */
        $owner = $this->owner
            ->where('id', $id)
            ->first();

        $owner->delete();

        return new JsonResponse(true);
    }

    public function createSettingsAndEmailDetails($ownerId) {
        // settings
        DB::table('settings')->insert([
            'name' => 'btc_fee',
            'value' => '0.001',
            'owner_id' => $ownerId
        ]);

        DB::table('settings')->insert([
            'name' => 'eth_fee',
            'value' => '0.03',
            'owner_id' => $ownerId
        ]);

        DB::table('settings')->insert([
            'name' => 'email_confirmation_text',
            'value' => 'To validate your email click on the button below',
            'owner_id' => $ownerId
        ]);

        DB::table('settings')->insert([
            'name' => 'btc_max_fee',
            'value' => '0.001',
            'owner_id' => $ownerId
        ]);

        DB::table('settings')->insert([
            'name' => 'eth_max_fee',
            'value' => '0.03',
            'owner_id' => $ownerId
        ]);

        DB::table('settings')->insert([
            'name' => 'email_name',
            'value' => 'EasyGram',
            'owner_id' => $ownerId
        ]);

        //email details
        DB::table('email_details')->insert([
            'text' => 'Some Text',
            'title' => 'Some title',
            'button_text' => 'button_text',
            'type' => 'email_confirmation',
            'subject' => 'Subject',
            'owner_id' => $ownerId
        ]);

        DB::table('email_details')->insert([
            'text' => 'Some Text',
            'title' => 'Some title',
            'button_text' => 'button_text',
            'type' => 'password_reset',
            'subject' => 'Subject',
            'owner_id' => $ownerId
        ]);


        DB::table('email_details')->insert([
            'text' => 'Some Text',
            'title' => 'Some title',
            'type' => 'subscription_ends',
            'subject' => 'Subject',
            'owner_id' => $ownerId
        ]);

    }

}