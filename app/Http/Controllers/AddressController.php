<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Owner;
use App\Models\Transaction;
use App\Repositories\AddressRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;

/**
 * Class AddressController
 * @package App\Http\Controllers
 */
class AddressController extends Controller
{
    /**
     * @var AddressRepository
     */
    protected $addressRepository;


    public function __construct(AddressRepository $addressRepository)
    {
        $this->addressRepository = $addressRepository;

        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN|ROLE_SUPER_ADMIN', [
            'except' => 'getAddressForUser'
        ]);
    }

    /**
     * @return array
     */
    public function getAddresses()
    {
        $addresses = $this->addressRepository->all();

        return compact('addresses');
    }

    /**
     * @param int $id
     * @return array
     */
    public function getAddress(int $id)
    {
        $address = $this->addressRepository->get($id);

        return compact('address');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getAddressForUser(Request $request)
    {
        $ownerId = auth()->user()->owner_id;

        $type = $request->only('type');

        $checkingTime = Carbon::now()->subMinutes(10);

        $lastTransactions = Transaction::where('type', strtoupper($type['type']))
            ->where('created_at','>=', $checkingTime)->get();

        $addresses = $this->addressRepository->getAllByType($type['type'], $ownerId);

        foreach ($addresses as $key => $address) {
            foreach ($lastTransactions as $lastTransaction) {
                if ($lastTransaction->address->value === $address->value) {
                    unset($addresses[$key]);
                }
            }
        }

        return compact('addresses');
    }

    /**
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function createAddress(Request $request)
    {
        try {
            $addressModel = new Address();

            $data = $request->only($addressModel->getFillable());

            $address = $this->addressRepository->create($data);

            return compact('address');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAddress(int $id, Request $request)
    {
        try {
            $addressModel = new Address();

            $data = $request->only($addressModel->getFillable());

            $address = $this->addressRepository->update($id, $data);

            return compact('address');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function deleteAddress(int $id)
    {
        try {
            $this->addressRepository->delete($id);

            return new JsonResponse(true);
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage());
        }

    }

}
