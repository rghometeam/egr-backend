<?php

namespace App\Http\Controllers;

use App\Models\BlackList;
use App\Models\Owner;
use App\Repositories\BlackListRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class BlackListController extends Controller
{
    /**
     * @var BlackListRepository
     */
    protected $blackListRepository;


    public function __construct(BlackListRepository $blackListRepository)
    {
        $this->blackListRepository = $blackListRepository;

        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN|ROLE_SUPER_ADMIN');
    }

    /**
     * @return array
     */
    public function getBlackLists()
    {
        $black_lists = $this->blackListRepository->all($this->getOwnerId());

        return compact('black_lists');
    }

    /**
     * @param int $id
     * @return array
     */
    public function getBlackList(int $id)
    {
        $black_list = $this->blackListRepository->get($id);

        return compact('black_list');
    }

    /**
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function createBlackList(Request $request)
    {
        try
        {
            $this->validate($request, $this->rules());

            $blackListModel = new BlackList();

            $data = $request->only($blackListModel->getFillable());

            $data['owner_id'] = $this->getOwnerId();

            $black_list = $this->blackListRepository->create($data);

            return compact('black_list');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function updateBlackList(int $id, Request $request)
    {
        try
        {
            $blackListModel = new BlackList();

            $data = $request->only($blackListModel->getFillable());

            $black_list = $this->blackListRepository->update($id, $data);

            return compact('black_list');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function deleteBlackList(int $id)
    {
        try
        {
            $this->blackListRepository->delete($id);

            return new JsonResponse(true);
        } catch (\Exception $exception)
        {
            return new JsonResponse($exception->getMessage());
        }

    }

    /**
     * @return array
     */
    private function rules()
    {
        return [
            'tx_id' => 'required',
        ];
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        $userId = auth()->user()->id;

        /**
         * @var Owner $owner
         */
        $owner = Owner::where('user_id', $userId)->first();

        return $owner['id'];
    }
}
