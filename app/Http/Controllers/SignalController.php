<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\Signal;
use App\Repositories\PackageRepository;
use App\Repositories\SignalRepository;
use App\Repositories\TargetRepository;
use App\Role;
use App\Services\PyrogramService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SignalController extends Controller
{
    /**
     * @var SignalRepository
     */
    protected $signalRepository;

    /**
     * @var TargetRepository
     */
    protected $targetRepository;

    /**
     * @var PyrogramService
     */
    protected $pyrogramService;

    /**
     * SignalController constructor.
     *
     * @param SignalRepository $signalRepository
     * @param TargetRepository $targetRepository
     * @param PyrogramService $pyrogramService
     */
    public function __construct(
        SignalRepository $signalRepository,
        TargetRepository $targetRepository,
        PyrogramService $pyrogramService
    ) {
        $this->signalRepository = $signalRepository;

        $this->targetRepository = $targetRepository;

        $this->pyrogramService = $pyrogramService;

        $this->middleware('auth');
        $this->middleware('role:ROLE_ADMIN|ROLE_SUPER_ADMIN|ROLE_EDITOR');
    }

    /**
     * @param $status
     * @return array
     */
    public function getSignalsByStatus($status)
    {
        $signals = $this->signalRepository->all($status);

        return compact('signals');
    }

    /**
     * @param int $id
     * @return array
     */
    public function getSignal(int $id)
    {
        $signal = $this->signalRepository->get($id);

        return compact('signal');
    }


    /**
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function createSignal(Request $request)
    {
        try {
            $this->validate($request, $this->rules());

            $signalModel = new Signal();

            $data = $request->only($signalModel->getFillable());

            $signal = $this->signalRepository->create($data);

            $targetsData = $request->only('targets');

            $dataTargetsToSave = [];
            foreach ($targetsData['targets'] as $target) {
                array_push($dataTargetsToSave,
                    [
                        'number' => $target['number'],
                        'value' => $target['value'],
                        'signal_id' => $signal->id
                    ]
                );
            }

            $this->targetRepository->create($dataTargetsToSave);

            $signal = $this->signalRepository->get($signal->id);

            $publish = $this->pyrogramService->publishSignal($signal);

            $this->signalRepository->update($signal->id, ['reply_id' => $publish['message_id']]);

            $signal = $this->signalRepository->get($signal->id);

            return compact('signal');
        } catch (ValidationException $e) {
            return new JsonResponse($e->getMessage());
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function updateSignal(int $id, Request $request)
    {
        try {
            $signalModel = new Signal();

            $data = $request->only($signalModel->getFillable());

            $signal = $this->signalRepository->update($id, $data);

            if ($signal->status === 'canceled') {
                $this->pyrogramService->cancelSignal($signal);
            }

            $targetsData = $request->only('targets');

            if (isset($targetsData['targets'])) {
                foreach ($targetsData['targets'] as $target) {

                    if (isset($target['id'])) {
                        $this->targetRepository->update($target['id'], ['value' => $target['value']]);
                    } else {
                        $this->targetRepository->create([
                            'number' => $target['number'],
                            'value' => $target['value'],
                            'signal_id' => $signal->id
                        ]);
                    }

                }
            }

            $signal = $this->signalRepository->get($signal->id);

            $this->pyrogramService->updateSignal($signal);

            return compact('signal');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function deleteSignal(int $id)
    {
        try {
            $this->signalRepository->delete($id);

            return new JsonResponse(true);
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage());
        }
    }

    /**
     * @return array
     */
    private function rules()
    {
        return [
            'market' => 'required',
            'pair' => 'required',
            'buy_zone_one' => 'required',
            'buy_zone_two' => 'required',
            'status' => 'required',
            'targets' => 'required'
        ];
    }
}
