<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class RoleControllerController
 * @package App\Http\Controllers
 */
class RoleControllerController extends Controller
{
    /**
     * @var Role
     */
    protected $role;

    /**
     * RoleControllerController constructor.
     * @param Role $role
     */
    public function __construct(Role $role)
    {
        $this->role = $role;

        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN|ROLE_SUPER_ADMIN');
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->role->all();

        return compact('roles');
    }
}
