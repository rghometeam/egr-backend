<?php

namespace App\Http\Controllers;

use App\Models\BlackList;
use App\Models\Transaction;
use App\Repositories\TransactionRepository;
use App\Services\CryptoCurrency;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TransactionController extends Controller
{
    /**
     * @var TransactionRepository
     */
    protected $transactionRepository;

    /**
     * TransactionController constructor.
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;

        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN', [
            'except' => [
                'getTransactions', 'checkTransaction',
                'latestTransaction', 'getTransaction',
                'createTransaction', 'filter'
            ]
        ]);
    }


    /**
     * @return array
     */
    public function getTransactionsForAdmin()
    {
        $transactions = $this->transactionRepository->allForAdmin();

        return compact('transactions');
    }

    /**
     * @return array
     */
    public function getTransactions()
    {
        $transactions = $this->transactionRepository->all();

        return compact('transactions');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function filter(Request $request)
    {
        $data = $request->only(['search', 'status_id']);

        $transactions = $this->transactionRepository->filter($data);

        return compact('transactions');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function checkTransaction(Request $request, int $id)
    {
        return $this->transactionRepository->processTransaction($id);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function latestTransaction(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        /** @var Transaction $transaction */
        $transaction = $this->transactionRepository->getLatestByUser($user->id);

        if (!$transaction && $user->finished_date) {
            $transaction = [
                'status' => 1,
                'finished_date' => $user->finished_date,
                'updated_at' => $user->updated_at,
            ];
        }

        return compact('transaction', 'user');
    }

    /**
     * @param int $id
     * @return array
     */
    public function getTransaction(int $id)
    {
        $transaction = $this->transactionRepository->get($id);

        return compact('transaction');
    }

    /**
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function createTransaction(Request $request)
    {
        try {
            $transactionModel = new Transaction();

            $data = $request->only($transactionModel->getFillable());

            $data['user_id'] = auth()->user()->id;
            $data['owner_id'] = auth()->user()->owner_id;

            $blackList = BlackList::where('tx_id', $data['tx_id'])->first();

            if (isset($blackList)) {
                $transaction = 'black_list';
            } else {
                if (!$transaction = $this->transactionRepository->getUnique($data['tx_id'], $data['user_id'])) {

                    $transaction = $this->transactionRepository->create($data);
                    $this->transactionRepository->processTransaction($transaction->id);
                }
            }

            return compact('transaction');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function updateTransaction(int $id, Request $request)
    {
        try {
            $transactionModel = new Transaction();

            $data = $request->only($transactionModel->getFillable());

            $transaction = $this->transactionRepository->update($id, $data);

            return compact('transaction');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function deleteTransaction(int $id)
    {
        try {
            $this->transactionRepository->delete($id);

            return new JsonResponse(true);
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage());
        }

    }

}
