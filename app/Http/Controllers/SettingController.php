<?php

namespace App\Http\Controllers;

use App\Exports\GroupMembers;
use App\Models\Setting;
use App\Repositories\LogRepository;
use App\Repositories\SettingRepository;
use App\Repositories\UserRepository;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use GuzzleHttp\Client;
use App\Exports\PremiumMembers;

/**
 * Class SettingController
 * @package App\Http\Controllers
 */
class SettingController extends Controller
{
    /**
     * @var SettingRepository
     */
    protected $settingRepository;

    /**
     * @var UserRepository
     */
    protected $usersRepository;

    /**
     * SettingController constructor.
     *
     * @param SettingRepository $settingRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        SettingRepository $settingRepository,
        UserRepository $userRepository
    )
    {
        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN|ROLE_SUPER_ADMIN', [
            'except' => [ 'getSettings', 'getSetting' ]
        ]);

        $this->settingRepository = $settingRepository;
        $this->usersRepository = $userRepository;
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        $settings = $this->settingRepository->all();

        return compact('settings');
    }

    /**
     * @return array
     */
    public function getSettingsForAdmin()
    {
        $settings = $this->settingRepository->getSettingsForAdminPay();

        return compact('settings');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getSetting(int $id)
    {
        $setting = $this->settingRepository->get($id);

        return compact('setting');
    }

    /**
     * @param int $id
     * @param Request $request
     * @return \App\Models\Setting|null|\Symfony\Component\HttpFoundation\Response
     */
    public function updateSetting(int $id, Request $request)
    {
        try {
            $settingModel = new Setting();

            $data = $request->only($settingModel->getFillable());

            $setting = $this->settingRepository->update($id, $data);

            return compact('setting');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param Request $request
     * @return array|string
     */
    public function cryptogramSync(Request $request)
    {
        $preparedEmails = [];
        $users = $this->usersRepository->exportForCryptogram();
        foreach ($users as $user) {
            $preparedEmails[] = $user->email;
        }

//        $url = 'https://httpbin.org/post';
        $url = 'https://cryptogram.co.il/rios489rtv/BitcollegePremium.aspx';
        $guzzle = new Client();

        try {
            $guzzle->post($url, [
                RequestOptions::JSON => $preparedEmails
            ]);

            $tmp = [];
            foreach ($preparedEmails as $datum) {
                $tmp[] = [$datum];
            }

            return (new GroupMembers($tmp))->download('result.xlsx');
        } catch (\Exception $exception) {
            LogRepository::setError('cryptogram', $exception->getMessage());

            return new JsonResponse(['error' => $exception->getMessage()], 500);
        }
    }
    

}
