<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Repositories\FaqRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class FaqController
 * @package App\Http\Controllers
 */
class FaqController extends Controller
{
    /**
     * @var FaqRepository
     */
    protected $faqRepository;

    /**
     * FaqController constructor.
     * @param FaqRepository $faqRepository
     */
    public function __construct(FaqRepository $faqRepository)
    {
        $this->faqRepository = $faqRepository;

        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN|ROLE_SUPER_ADMIN', [
            'except' => 'getFaqs'
        ]);
    }

    /**
     * @return array
     */
    public function getFaqs()
    {
        $faqs = $this->faqRepository->all();

        return compact('faqs');
    }

    /**
     * @param int $id
     * @return array
     */
    public function getFaq(int $id)
    {
        $faq = $this->faqRepository->get($id);

        return compact('faq');
    }

    /**
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function createFaq(Request $request)
    {
        try {
            $faqModel = new Faq();

            $data = $request->only($faqModel->getFillable());

            $faq = $this->faqRepository->create($data);

            return compact('faq');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function updateFaq(int $id, Request $request)
    {
        try {
            $faqModel = new Faq();

            $data = $request->only($faqModel->getFillable());

            $faq = $this->faqRepository->update($id, $data);

            return compact('faq');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function deleteFaq(int $id)
    {
        try {
            $this->faqRepository->delete($id);

            return new JsonResponse(true);
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage());
        }

    }
}
