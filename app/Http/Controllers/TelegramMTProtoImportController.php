<?php

namespace App\Http\Controllers;

use App\Services\MTProtoService;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use App\Exports\GroupMembers;

class TelegramMTProtoImportController extends Controller
{

    /**
     * TelegramMTProtoAuthController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function sendGroup(Request $request)
    {
        $client = new Client();
        $data = $request->only(['group', 'option']);

        if (strpos($data['group'], 'joinchat/') === false) {
            $data['group'] = trim(parse_url($data['group'])['path'], '/');
        }

        $response = $client->post('localhost:82/users', [
            RequestOptions::JSON => $data
        ]);

        $data = \GuzzleHttp\json_decode($response->getBody()->getContents());

        return (new GroupMembers($data))->download('result.xlsx');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function importGroupMembers(Request $request)
    {
        $client = new Client();
        $data = $request->only(['group']);

        $client->post('localhost:83/bulk/import-members', [
            RequestOptions::JSON => $data
        ]);

        return new JsonResponse(true);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getBulkMessagesStatistic(Request $request)
    {
        $client = new Client();
        $response = $client->post('localhost:83/bulk/statistic');

        $statistic = \GuzzleHttp\json_decode($response->getBody()->getContents());

        return compact('statistic');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getMessages(Request $request)
    {
        $client = new Client();
        $response = $client->get('localhost:83/bulk/get-messages');

        return new JsonResponse(\GuzzleHttp\json_decode($response->getBody()->getContents()));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getMeta(Request $request)
    {
        $client = new Client();
        $response = $client->get('localhost:83/bulk/get-meta');

        return new JsonResponse(\GuzzleHttp\json_decode($response->getBody()->getContents()));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function saveMessages(Request $request)
    {
        $data = $request->only(['messages']);
        $client = new Client();
        $client->post('localhost:83/bulk/save-messages', [
            RequestOptions::JSON => $data
        ]);

        return new JsonResponse(true);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function saveMeta(Request $request)
    {
        $data = $request->only(['delay']);
        $client = new Client();
        $client->post('localhost:83/bulk/save-meta', [
            RequestOptions::JSON => $data
        ]);

        return new JsonResponse(true);
    }

}
