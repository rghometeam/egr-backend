<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 07.02.2018
 * Time: 14:07
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use JavaScript;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('pages.builder.index');
    }

    /**
     * @return Factory|View
     */
    public function browser()
    {
        return view('browser');
    }

    /**
     * @return Factory|View
     */
    public function createGet()
    {
        JavaScript::put([
            'edit' => false,
        ]);

        return view('pages.builder.create');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function editGet(Request $request, int $id)
    {
        JavaScript::put([
            'edit' => true,
            'id' => $id
        ]);

        return view('pages.builder.create');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function loginGet(Request $request)
    {
        return view('pages.login');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function loginOldGet(Request $request)
    {
        return view('pages.login-old');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function registerGet(Request $request)
    {
        return view('pages.register');
    }

    /**
     * @return Factory|View
     */
    public function usersIndex()
    {
        return view('pages.users.index');
    }

    /**
     * @return Factory|View
     */
    public function userCreate()
    {
        JavaScript::put([
            'edit' => false,
        ]);

        return view('pages.users.create');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function userEdit(Request $request, int $id)
    {
        JavaScript::put([
            'edit' => true,
            'id' => $id
        ]);

        return view('pages.users.create');
    }

    /**
     * @return Factory|View
     */
    public function packagesIndex()
    {
        return view('pages.packages.index');
    }

    /**
     * @return Factory|View
     */
    public function telegramAuthIndex()
    {
        return view('pages.telegram-auth.index');
    }

    /**
     * @return Factory|View
     */
    public function telegramImportIndex()
    {
        return view('pages.telegram-import.index');
    }

    /**
     * @return Factory|View
     */
    public function packageCreate()
    {
        JavaScript::put([
            'edit' => false,
        ]);

        return view('pages.packages.create');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function packageEdit(Request $request, int $id)
    {
        JavaScript::put([
            'edit' => true,
            'id' => $id
        ]);

        return view('pages.packages.create');
    }



    /**
     * @return Factory|View
     */
    public function blackListIndex()
    {
        return view('pages.black-list.index');
    }

    /**
     * @return Factory|View
     */
    public function blackListCreate()
    {
        JavaScript::put([
            'edit' => false,
        ]);

        return view('pages.black-list.create');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function blackListEdit(Request $request, int $id)
    {
        JavaScript::put([
            'edit' => true,
            'id' => $id
        ]);

        return view('pages.black-list.create');
    }

    /**
     * @return Factory|View
     */
    public function dashboardIndex()
    {
        return view('pages.dashboard.index');
    }

    /**
     * @return Factory|View
     */
    public function settingsIndex()
    {
        return view('pages.settings.index');
    }

    /**
     * @return Factory|View
     */
    public function emailDetailsIndex()
    {
        return view('pages.email-details.index');
    }

    /**
     * @return Factory|View
     */
    public function emailDetailEdit(Request $request, int $id)
    {
        JavaScript::put([
            'edit' => true,
            'id' => $id
        ]);

        return view('pages.email-details.edit');
    }

    /**
     * @return Factory|View
     */
    public function logsIndex()
    {
        return view('pages.logs.index');
    }

    /**
     * @return Factory|View
     */
    public function faqUserIndex()
    {
        return view('pages.faq-user.index');
    }

    /**
     * @return Factory|View
     */
    public function faqAdminIndex()
    {
        return view('pages.faq-admin.index');
    }

    /**
     * @return Factory|View
     */
    public function faqAdminCreate()
    {
        JavaScript::put([
            'edit' => false,
        ]);

        return view('pages.faq-admin.create');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function faqAdminEdit(Request $request, int $id)
    {
        JavaScript::put([
            'edit' => true,
            'id' => $id
        ]);

        return view('pages.faq-admin.create');
    }

    /**
     * @return Factory|View
     */
    public function transactionsIndex()
    {
        return view('pages.transactions.index');
    }

    /**
     * @return Factory|View
     */
    public function referralsIndex()
    {
        return view('pages.referrals.index');
    }

    /**
     * @return Factory|View
     */
    public function withdrawalIndex()
    {
        return view('pages.withdrawal.index');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function transactionsEdit(Request $request, int $id)
    {
        JavaScript::put([
            'edit' => true,
            'id' => $id
        ]);

        return view('pages.transactions.edit');
    }

    /**
     * @return Factory|View
     */
    public function profileIndex()
    {
        JavaScript::put([
            'edit' => true
        ]);

        return view('pages.profile');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function settingsCreate(Request $request, int $id)
    {
        JavaScript::put([
            'edit' => true,
            'id' => $id
        ]);

        return view('pages.settings.create');
    }

    /**
     * @param Request $request
     * @param int $id
     * @param $token
     * @return Factory|View
     */
    public function confirm(Request $request, int $id, $token)
    {
        JavaScript::put([
            'id' => $id,
            'token' => $token
        ]);

        return view('pages.confirmation');
    }

    /**
     * @param Request $request
     * @param int $id
     * @param $token
     * @return Factory|View
     */
    public function newPassword(Request $request, int $id, $token)
    {
        JavaScript::put([
            'id' => $id,
            'token' => $token
        ]);

        return view('pages.new-password');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function reset(Request $request)
    {
        return view('pages.reset');
    }

    /**
     * @return Factory|View
     */
    public function addressesIndex()
    {
        return view('pages.addresses.index');
    }

    /**
     * @return Factory|View
     */
    public function addressesCreate()
    {
        JavaScript::put([
            'edit' => false,
        ]);

        return view('pages.addresses.create');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function addressesEdit(Request $request, int $id)
    {
        JavaScript::put([
            'edit' => true,
            'id' => $id
        ]);

        return view('pages.addresses.create');
    }
}