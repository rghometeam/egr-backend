<?php

namespace App\Http\Controllers;

use App\Models\Entity;
use App\Repositories\LogRepository;
use App\Role;
use App\User;
use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use App\Conversations\ExampleConversation;

class BotManController extends Controller
{
    private $welcomeText = 'Welcome Message';

    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');


        $botman->hears('/start', function (BotMan $bot) {

                $this->sendMessageAndInlineButtons($bot, $this->getStartData(), $this->welcomeText);

        });

        $botman->hears('button:([0-9]+)', function (BotMan $bot, $buttonId) {


                $this->deleteLastMessage($bot);

                $entity = $this->getEntity($buttonId);

                $this->sendMessageAndInlineButtons($bot, $entity['data'], (isset($entity['message'])) ? $entity['message'] : $this->welcomeText);

        });

        $botman->hears('back:([0-9]+)', function (BotMan $bot, int $buttonId) {

                $this->deleteLastMessage($bot);

                $this->sendMessageAndInlineButtons($bot, $this->getStartData(), $this->welcomeText);

        });


        try {
            $botman->listen();
        } catch (\Exception $exception) { }
    }


    public function sendMessageAndInlineButtons (BotMan $bot, array $inline_keyboards, $messageText)
    {
        $keyboard = array("inline_keyboard" => $inline_keyboards, 'one_time_keyboard' => false, 'resize_keyboard' => true);

        $replyMarkup = json_encode($keyboard);

        $bot->sendRequest( "sendDocument", [
            'caption' => $messageText,
            'document' => env('WELCOME_IMG'),
            'reply_markup' => $replyMarkup
        ]);

    }

    private function getStartData()
    {
        $entities = Entity::where('parent_id', null)
            ->get()
        ;

        return $this->prepareData($entities);
    }

    private function getEntity($id)
    {
        $entity = Entity::where('id', $id)->with('children')->first();

        $preparedData = $this->prepareData($entity->children);
        $preparedData[] = [
            [
                'text' => '🔙 חזור',
                'callback_data' => 'back:0'
            ]
        ];

        return [
            'data' => $preparedData,
            'message' => $entity->welcome
        ];
    }

    private function prepareData($entities)
    {
        $result = [];

        /**
         * @var int $index
         * @var Entity $entity
         */
        foreach ($entities as $index => $entity) {
            $tmp = [
                'text' => $entity->title,
            ];


            if ($entity->type === 'link') {
                $tmp['url'] = $entity->link;
            }

            if ($entity->type === 'button') {
                $tmp['callback_data'] = 'button:' . $entity->id;
            }

            $result[$index] = [ $tmp ];
        }

        return $result;
    }

    public function deleteLastMessage(BotMan $bot) {

        $bot->sendRequest( "deleteMessage", [
            "chat_id" => $bot->getMessage()->getPayload()['chat']['id'],
            "message_id" => $bot->getMessage()->getPayload()['message_id']
        ]);

    }
}
