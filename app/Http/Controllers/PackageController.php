<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Repositories\PackageRepository;
use App\Role;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PackageController extends Controller
{
    /**
     * @var PackageRepository
     */
    protected $packageRepository;

    /**
     * PackageController constructor.
     * @param PackageRepository $packageRepository
     */
    public function __construct(PackageRepository $packageRepository)
    {
        $this->packageRepository = $packageRepository;

        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN|ROLE_SUPER_ADMIN', [
            'except' => [ 'getPackages', 'getPackage' ]
        ]);
    }

    /**
     * @return array
     */
    public function getPackages()
    {
        $packages = $this->packageRepository->all();

        $user = auth()->user();

        if ((bool) $user->need_discount === true) {
            foreach ($packages as &$package) {
                $package->Price_BTC = ($package->Price_BTC - ($package->Price_BTC / 100) * 5);
            }
        }


        return compact('packages');
    }

    /**
     * @return array
     */
    public function getPackagesForAdmin()
    {
        $packages = $this->packageRepository->getPackagesForAdmin();

        $user = auth()->user();

        if ((bool) $user->need_discount === true) {
            if (isset($packages['for_admin'])) {
                foreach ($packages['for_admin'] as &$package) {
                    $package->Price_BTC = ($package->Price_BTC - ($package->Price_BTC / 100) * 5);
                }
            }
        }


        return compact('packages');
    }

    /**
     * @param int $id
     * @return array
     */
    public function getPackage(int $id)
    {
        $package = $this->packageRepository->get($id);
        return compact('package');
    }

    /**
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function createPackage(Request $request)
    {
        try {
            $this->validate($request, $this->rules());

            $packageModel = new Package();

            $data = $request->only($packageModel->getFillable());

            $package = $this->packageRepository->create($data);

            return compact('package');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function updatePackage(int $id, Request $request)
    {
        try {
            $packageModel = new Package();

            $data = $request->only($packageModel->getFillable());

            $package = $this->packageRepository->update($id, $data);

            return compact('package');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function deletePackage(int $id)
    {

        try {
            $this->packageRepository->delete($id);

            return new JsonResponse(true);
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage());
        }

    }

    /**
     * @return array
     */
    private function rules()
    {
        return [
            'name' => 'required',
//            'description' => 'required',
            'days' => 'required'
        ];
    }
}
