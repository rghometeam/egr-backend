<?php

namespace App\Http\Controllers;

use App\Models\Owner;
use App\Repositories\OwnerRepository;
use App\Repositories\UserRepository;
use App\Role;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var
     */
    protected $ownerRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     * @param OwnerRepository $ownerRepository
     */
    public function __construct(UserRepository $userRepository, OwnerRepository $ownerRepository)
    {
        $this->userRepository = $userRepository;

        $this->ownerRepository = $ownerRepository;

        $this->middleware('auth');

        $this->middleware('role:ROLE_SUPER_ADMIN|ROLE_ADMIN', [
            'except' => [
                'profile', 'updateProfile', 'balance'
            ]
        ]);
    }

    /**
     * @return array
     */
    public function balance()
    {
        $balance = auth()->user()->balance;

        return compact('balance');
    }

    /**
     * @return array
     */
    public function getAdmins()
    {
        $admins = $this->userRepository->getAdmins();

        return compact('admins');
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        $users = $this->userRepository->all();

        return compact('users');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function filter(Request $request)
    {
        $data = $request->only(['search', 'role_id', 'sortBy', 'sortDesc']);

        $users = $this->userRepository->filter($data);

        return compact('users');
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getUser(int $userId)
    {
        $user = $this->userRepository->get($userId);

        return compact('user');
    }

    /**
     * @return array
     */
    public function profile()
    {
        $user = $this->userRepository->get(auth()->user()->id);

        if (auth()->user()->roles[0]['id'] === Role::ROLE_ADMIN) {
            $user['owner_data'] = Owner::where('user_id', auth()->user()->id)->first();
        }

        return compact('user');
    }

    /**
     * @param Request $request
     * @return User|null|\Symfony\Component\HttpFoundation\Response
     */
    public function createUser(Request $request)
    {
        try
        {
//            $this->validate($request, $this->rules());

            $data = $request->all();

            $user = $this->userRepository->create($data);

            /**
             * var User $authUser
             */
            $authUser = User::where('id', auth()->user()->id)->with('roles')->first();

            if (Role::ROLE_SUPER_ADMIN === $authUser['roles'][0]['id']) {
                if ($data['roles'] === Role::ROLE_ADMIN) {
                    $this->ownerRepository->create([
                        'host' => null,
                        'channel_premium' => null,
                        'channel_log' => null,
                        'user_id' => $user['id']
                    ]);
                }
            }

            return $user;
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $userId
     * @param Request $request
     * @return User|null|\Symfony\Component\HttpFoundation\Response
     */
    public function updateUser(int $userId, Request $request)
    {
        try {
            $data = $request->all();

            return $this->userRepository->update($userId, $data);
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param Request $request
     * @return User|null|\Symfony\Component\HttpFoundation\Response
     */
    public function updateProfile(Request $request)
    {
        try {
            $data = $request->all();

            return $this->userRepository->update(auth()->user()->id, $data);
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $userId
     * @return mixed
     */
    public function deleteUser(int $userId)
    {
        return $this->userRepository->delete($userId);
    }

    /**
     * @return array
     */
    private function rules()
    {
        return [
            'email' => 'required',
            'role' => 'required'
        ];
    }
}
