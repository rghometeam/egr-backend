<?php

namespace App\Http\Controllers;

use App\Models\Owner;
use App\Repositories\OwnerRepository;
use App\Role;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class OwnerController extends Controller
{
    /**
     * @var OwnerRepository
     */
    protected $ownerRepository;

    /**
     * OwnerController constructor.
     * @param OwnerRepository $ownerRepository
     */
    public function __construct(OwnerRepository $ownerRepository)
    {
        $this->ownerRepository = $ownerRepository;

        $this->middleware('auth');

        $this->middleware('role:ROLE_SUPER_ADMIN');
    }

    /**
     * @return array
     */
    public function getOwners()
    {
        $owners = $this->ownerRepository->all();

        return compact('owners');
    }

    /**
     * @param int $id
     * @return array
     */
    public function getOwner(int $id)
    {
        $owner = $this->ownerRepository->get($id);

        return compact('owner');
    }

    /**
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function createOwner(Request $request)
    {
        try {
            $this->validate($request, $this->rules());

            $ownerModel = new Owner();

            $data = $request->only($ownerModel->getFillable());

            $owner = $this->ownerRepository->create($data);

            return compact('owner');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function updateOwner(int $id, Request $request)
    {
        try {
            $ownerModel = new Owner();

            $data = $request->only($ownerModel->getFillable());

            $owner = $this->ownerRepository->update($id, $data);

            return compact('owner');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function deleteOwner(int $id)
    {

        try {
            $this->ownerRepository->delete($id);

            return new JsonResponse(true);
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage());
        }

    }

    /**
     * @return array
     */
    private function rules()
    {
        return [
            'host' => 'required',
            'channel_log' => 'required',
            'channel_premium' => 'required'
        ];
    }
}
