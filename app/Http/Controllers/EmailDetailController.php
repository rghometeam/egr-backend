<?php

namespace App\Http\Controllers;

use App\Models\EmailDetail;
use App\Repositories\EmailDetailRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class EmailDetailController
 * @package App\Http\Controllers
 */
class EmailDetailController extends Controller
{
    /**
     * @var EmailDetailRepository
     */
    protected $emailDetailRepository;

    /**
     * EmailDetailController constructor.
     * @param EmailDetailRepository $emailDetailRepository
     */
    public function __construct(EmailDetailRepository $emailDetailRepository)
    {
        $this->emailDetailRepository = $emailDetailRepository;

        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN|ROLE_SUPER_ADMIN');
    }

    /**
     * @return array
     */
    public function getEmailDetails()
    {
        $details = $this->emailDetailRepository->all();

        return compact('details');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getEmailDetail(int $id)
    {
        $detail = $this->emailDetailRepository->get($id);

        return compact('detail');
    }

    /**
     * @param int $id
     * @param Request $request
     * @return \App\Models\Setting|null|\Symfony\Component\HttpFoundation\Response
     */
    public function updateEmailDetail(int $id, Request $request)
    {
        try {
            $emailDetailModel = new EmailDetail();

            $data = $request->only($emailDetailModel->getFillable());

            $detail = $this->emailDetailRepository->update($id, $data);

            return compact('detail');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

}
