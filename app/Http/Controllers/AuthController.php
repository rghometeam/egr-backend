<?php

namespace App\Http\Controllers;

use App\Models\Owner;
use App\Models\PasswordReset;
use App\Notifications\ConfirmEmail;
use App\Repositories\LogRepository;
use App\Repositories\UserRepository;
use App\Services\PyrogramService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Role;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Auth\AuthManager;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    private $pyrogramService;

    private $adminTelegramName = 'gorhome';

    public function __construct(
        UserRepository $userRepository,
        PyrogramService $pyrogramService
    )
    {
        $this->middleware('auth:api',
            [
                'except' =>
                    [
                        'login',
                        'loginOld',
                        'register',
                        'confirmation',
                        'reset',
                        'newPassword',
                        'checkUsername',
                        'checkPin',
                        'changePhone'
                    ]
            ]
        );

        $this->userRepository = $userRepository;
        $this->pyrogramService = $pyrogramService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function checkUsername(Request $request)
    {
        $userName = $request->get('username');

        try {
            User::whereTelegramName($userName)->firstOrFail();


            return new JsonResponse(false);
        } catch (\Exception $exception) {
            return new JsonResponse(true);
        }
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function changePhone(Request $request)
    {
        $phone = $request->get('phone');
        $user = auth()->user();
        $user->phone = $phone;
        $user->save();

        $this->userRepository->setUserAuthCode($user);

        if ($response = $this->pyrogramService->sendCode($user)) {
            if (isset($response['users'])) {
                $this->userRepository->updateUserByTelegramInfo($user, $response['users'][0]);
            }

            return new JsonResponse(true);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function confirmation(Request $request)
    {
        $credentials = request(['id', 'token']);

        try {
            $user = User::whereId($credentials['id'])
                ->whereConfirmationCode($credentials['token'])
                ->firstOrFail()
            ;

            $user->confirmation_code = null;
            $user->confirmed = true;
            $user->save();

            return new JsonResponse(true);
        } catch (\Exception $e) {
            if ($e->getMessage() === 'No query results for model [App\User].')
                LogRepository::setError('auth', \GuzzleHttp\json_encode([
                    'message' => 'Confirmation code doesnt exists',
                    'payload' => 'user: ' . User::whereId($credentials['id'])->first()
                ]));
            return new JsonResponse(false);
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginOld()
    {
        $credentials = request(['email', 'password']);
        $credentials['confirmed'] = true;

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        fwrite(fopen('/tmp/dump', 'w'), print_r(request(['phone', 'ref', 'admin', 'host']), 1));
        $credentials = request(['phone', 'ref', 'admin', 'host']);
        $credentials['confirmed'] = true;

        try {
            $user = User::wherePhone($credentials['phone'])->firstOrFail();
        } catch (\Exception $exception) {
            $user = new User();
            $user->phone = $credentials['phone'];
            $user->password = bcrypt($credentials['phone']);
            $user->confirmed = true;
            $user->affiliate_id = $this->userRepository->getRandomCode('affiliate_id');

            if (isset($credentials['ref'])) {
                $refferedUser = User::whereAffiliateId($credentials['ref'])->first();

                if ($refferedUser) {
                    $user->referred_by = $refferedUser->id;
                    $user->need_discount = true;
                }
            }

            if ($credentials['admin'] == 0) {

                /**
                 * @var Owner $owner
                 */
                $owner = Owner::where('host', $credentials['host'])->first();

                $user->owner_id = $owner['id'];
            }

            $user->save();

            if ( $credentials['admin'] == 1) {
                $user = User::where('id', $user->id)->first();
                $user->attachRole(Role::ROLE_ADMIN);
                /**
                 * @var Owner $owner
                 */
                $owner = Owner::create(['user_id' => $user->id]);

                $mainSuperAdmin = User::where('telegram_name', $this->adminTelegramName)->first();
                $mainAdminOwner = Owner::where('user_id', $mainSuperAdmin['id'])->first();
                $user->owner_id = $mainAdminOwner['id'];

                $user->save();

                $this->createSettingsAndEmailDetails($owner['id']);
            } else {
                $user->attachRole(Role::ROLE_USER);

            }
        }

        $this->userRepository->setUserAuthCode($user);

//        return new JsonResponse(true);

        if ($response = $this->pyrogramService->sendCode($user)) {
            if (isset($response['users'])) {
                $this->userRepository->updateUserByTelegramInfo($user, $response['users'][0]);

                return new JsonResponse(['status' => true]);
            } else {
                return new JsonResponse(['status' => false, 'error' => $response['error']]);
            }
        }

        return response()->json(['error' => 'Unauthorized'], 401);

//        if (!$token = auth()->attempt($credentials)) {
//            return response()->json(['error' => 'Unauthorized'], 401);
//        }
//
//        return $this->respondWithToken($token);
    }

    public function checkPin(Request $request)
    {
        $credentials = request(['phone', 'code']);

        try {
            $user = User::wherePhone($credentials['phone'])
                ->whereAuthCode($credentials['code'])
                ->firstOrFail()
            ;

            $user->auth_code = null;
            $user->save();

            return $this->respondWithToken(\JWTAuth::fromUser($user));
        } catch (\Exception $exception) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    /**
     * @param $ownerId
     */
    public function createSettingsAndEmailDetails($ownerId) {
        // settings
        DB::table('settings')->insert([
            'name' => 'btc_fee',
            'value' => '0.001',
            'owner_id' => $ownerId
        ]);

        DB::table('settings')->insert([
            'name' => 'eth_fee',
            'value' => '0.03',
            'owner_id' => $ownerId
        ]);

        DB::table('settings')->insert([
            'name' => 'email_confirmation_text',
            'value' => 'To validate your email click on the button below',
            'owner_id' => $ownerId
        ]);

        DB::table('settings')->insert([
            'name' => 'btc_max_fee',
            'value' => '0.001',
            'owner_id' => $ownerId
        ]);

        DB::table('settings')->insert([
            'name' => 'eth_max_fee',
            'value' => '0.03',
            'owner_id' => $ownerId
        ]);

        DB::table('settings')->insert([
            'name' => 'email_name',
            'value' => 'EasyGram',
            'owner_id' => $ownerId
        ]);

        //email details
        DB::table('email_details')->insert([
            'text' => 'Some Text',
            'title' => 'Some title',
            'button_text' => 'button_text',
            'type' => 'email_confirmation',
            'subject' => 'Subject',
            'owner_id' => $ownerId
        ]);

        DB::table('email_details')->insert([
            'text' => 'Some Text',
            'title' => 'Some title',
            'button_text' => 'button_text',
            'type' => 'password_reset',
            'subject' => 'Subject',
            'owner_id' => $ownerId
        ]);


        DB::table('email_details')->insert([
            'text' => 'Some Text',
            'title' => 'Some title',
            'type' => 'subscription_ends',
            'subject' => 'Subject',
            'owner_id' => $ownerId
        ]);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function newPassword(Request $request)
    {
        $data = $request->only(['password', 'id', 'token']);

        try {
            $passwordReset = PasswordReset::whereToken($data['token'])->first();

            $user = User::whereEmail($passwordReset->email)->first();
            $user->password = bcrypt($data['password']);
            $user->save();

            $passwordReset->delete();

            return new JsonResponse(true);
        } catch (\Exception $exception) {
            return new JsonResponse(false);
        }

    }

    /**
     * @return JsonResponse
     */
    public function reset()
    {
        $email = request('email');

        try {
            /** @var User $user */
            $user = User::whereEmail($email)->first();

            $passwordReset = new PasswordReset();
            $passwordReset->email = $email;
            $passwordReset->token = str_random(30);
            $passwordReset->save();

            $user->notify(new \App\Notifications\PasswordReset($passwordReset));

            return new JsonResponse(true);
        } catch (\Exception $e) {
            return new JsonResponse(false);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        try {
            $user = new User();

            $data = $request->only($user->getFillable());

            $data['password'] = bcrypt($data['password']);
            $data['confirmation_code'] = str_random(30);

            $data['telegram_name'] = trim($data['telegram_name'],'@');

            $user->fill($data)->save();

            $user->attachRole(Role::ROLE_USER);

//            event(new Registered($user));
            $user->notify(new ConfirmEmail());

            return new JsonResponse(true);
        } catch (\Exception $exception) {
            LogRepository::setError('auth', \GuzzleHttp\json_encode([
                'message' => $exception->getMessage(),
                'payload' => $request->request->all()
            ]));

            return new JsonResponse(false);
        }

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = User::where('id', auth()->user()->id)
            ->with('roles')
            ->first()
        ;

        return response()->json($user);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}