<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 07.02.2018
 * Time: 14:55
 */

namespace App\Http\Controllers;


use App\Models\Entity;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getAllEntities()
    {
        $entities = Entity::with('parent')
            ->with('children')
            ->get()
        ;

        return compact('entities');
    }

    public function getButtonEntities()
    {
        $entities = Entity::where('type', 'button')
            ->with('parent')
            ->with('children')
            ->get()
        ;

        return compact('entities');
    }

    public function updateEntity(Request $request)
    {
        /** @var Entity $entity */
        $entity = Entity::where('id', $request->get('id'))->first();

        $data = $request->only($entity->getFillable());
        $entity->fill($data)->save();

        return compact('entity');
    }

    public function createEntity(Request $request)
    {
        $entity = new Entity();

        $data = $request->only($entity->getFillable());
        $entity->fill($data)->save();

        return compact('entity');
    }

    public function getEntity(Request $request, int $id)
    {
        $entity = Entity::with('parent')
            ->with('children')
            ->where('id', $id)
            ->first()
        ;

        return compact('entity');
    }

    public function deleteEntity(Request $request, int $id)
    {
        $entity = Entity::where('id', $id)->first();
        $entity->delete();

        return new JsonResponse(true);
    }
}