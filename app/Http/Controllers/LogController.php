<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Repositories\LogRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class LogController
 * @package App\Http\Controllers
 */
class LogController extends Controller
{
    /**
     * @var LogRepository
     */
    protected $logRepository;

    /**
     * LogController constructor.
     * @param LogRepository $logRepository
     */
    public function __construct(LogRepository $logRepository)
    {
        $this->logRepository = $logRepository;

        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN|ROLE_SUPER_ADMIN');
    }

    /**
     * @return array
     */
    public function getLogs()
    {
        $logs = $this->logRepository->all();

        return compact('logs');
    }

    /**
     * @param int $id
     * @return array
     */
    public function getLog(int $id)
    {
        $log = $this->logRepository->get($id);

        return compact('log');
    }

    /**
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function createLog(Request $request)
    {
        try {
            $this->validate($request, $this->rules());

            $logModel = new Log();

            $data = $request->only($logModel->getFillable());

            $log = $this->logRepository->create($data);

            return compact('log');
        } catch (ValidationException $e) {
            return $e->getResponse();
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return array|string
     */
    public function updateLog(int $id, Request $request)
    {
        try {
            $logModel = new Log();

            $data = $request->only($logModel->getFillable());

            $log = $this->logRepository->update($id, $data);

            return compact('log');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function deleteLog(int $id)
    {
        try {
            $this->logRepository->delete($id);

            return new JsonResponse(true);
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage());
        }

    }

    /**
     * @return array
     */
    private function rules()
    {
        return [
            'provider' => 'required',
            'status' => 'required',
            'entity_id' => 'required',
            'data' => 'required'
        ];
    }
}
