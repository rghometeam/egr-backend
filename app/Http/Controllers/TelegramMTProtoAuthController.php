<?php

namespace App\Http\Controllers;

use App\Services\MTProtoService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TelegramMTProtoAuthController extends Controller
{
    /**
     * @var MTProtoService
     */
    protected $protoService;

    /**
     * TelegramMTProtoAuthController constructor.
     * @param MTProtoService $protoService
     */
    public function __construct(MTProtoService $protoService)
    {
        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN');

        $this->protoService = $protoService;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function sendPhone(Request $request)
    {
        $data = $request->only('phone');

        try {
            $this->protoService->sentPhone($data['phone']);
            return ['status' => true];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        }

    }

    /**
     * @param Request $request
     * @return array
     */
    public function sendCode(Request $request)
    {
        $data = $request->only('code');

        try {
            $authorization = $this->protoService->sentCode($data['code']);

            return ['status' => true, 'authorization' => $authorization];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        }

    }

    /**
     * @param Request $request
     * @return array
     */
    public function sendPassword(Request $request)
    {
        $data = $request->only('password');

        try {
            $authorization = $this->protoService->sendPassword($data['password']);

            return ['status' => true, 'authorization' => $authorization];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage()
            ];
        }

    }

//    /**
//     * @param int $id
//     * @return array
//     */
//    public function getFaq(int $id)
//    {
//        $faq = $this->faqRepository->get($id);
//
//        return compact('faq');
//    }
//
//    /**
//     * @param Request $request
//     * @return array|null|\Symfony\Component\HttpFoundation\Response
//     */
//    public function createFaq(Request $request)
//    {
//        try
//        {
//            $faqModel = new Faq();
//
//            $data = $request->only($faqModel->getFillable());
//
//            $faq = $this->faqRepository->create($data);
//
//            return compact('faq');
//        } catch (ValidationException $e) {
//            return $e->getResponse();
//        }
//    }

}
