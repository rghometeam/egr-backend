<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 21.04.2018
 * Time: 12:48
 */

namespace App\Http\Controllers;

use App\Models\ReferralTrack;
use App\Repositories\ReferralTrackRepository;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ReferralsController
 * @package App\Http\Controllers
 */
class ReferralsController extends Controller
{
    /**
     * @var ReferralTrackRepository
     */
    private $referralTrackRepository;

    /**
     * ReferralsController constructor.
     * @param ReferralTrackRepository $referralTrackRepository
     */
    public function __construct(ReferralTrackRepository $referralTrackRepository)
    {
        $this->referralTrackRepository = $referralTrackRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function track(Request $request)
    {
        $ip = $request->ip();
        $code = $request->get('ref');

        try {
            $this->referralTrackRepository->track($ip, $code);
        } catch (\Exception $exception) {}

        return new JsonResponse(true);
    }

    /**
     * @return JsonResponse
     */
    public function statistic()
    {
        $statistic = $this->referralTrackRepository->statistic(auth()->user());

        return compact('statistic');
    }

    /**
     * @return array
     */
    public function transactions()
    {
        $transactions = [];
        $payedUsers = $this->referralTrackRepository->getPayedUsers(auth()->user());

        foreach ($payedUsers as $user) {
            foreach ($user->transactions as $key => $transaction) {
                /** Just first transaction */
                if ($key > 0) continue;

                $transactions[] = $transaction;
            }
        }

        return compact('transactions');
    }
}