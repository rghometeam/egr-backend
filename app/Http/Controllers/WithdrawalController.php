<?php

namespace App\Http\Controllers;

use App\Models\BlackList;
use App\Models\Transaction;
use App\Models\Withdrawal;
use App\Repositories\TransactionRepository;
use App\Repositories\WithdrawalRepository;
use App\Services\CryptoCurrency;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class WithdrawalController
 * @package App\Http\Controllers
 */
class WithdrawalController extends Controller
{
    /**
     * @var WithdrawalRepository
     */
    protected $withdrawalRepository;

    /**
     * WithdrawalController constructor.
     * @param WithdrawalRepository $withdrawalRepository
     */
    public function __construct(WithdrawalRepository $withdrawalRepository)
    {
        $this->middleware('auth');

        $this->middleware('role:ROLE_ADMIN|ROLE_SUPER_ADMIN', [
            'except' => [
                'create', 'all'
            ]
        ]);

        $this->withdrawalRepository = $withdrawalRepository;
    }

    /**
     * @param Request $request
     * @return array|null|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $user = auth()->user();
        $data = $request->only((new Withdrawal())->getFillable());

        $data['from_user_id'] = $user->id;

        try {
            $withdrawal = $this->withdrawalRepository->create($data, $user);

            return compact('withdrawal');
        } catch (ValidationException $e) {
            return new JsonResponse(['error' => $e->validator->errors()->first()]);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function all(Request $request)
    {
        $withdrawals = $this->withdrawalRepository->all();

        return compact('withdrawals');
    }

}
